# rollbackのtest

## refs
- code
     - https://entgo.io/ja/docs/transactions/
- 2段構えのrollbackになっている。
     - 通常 → `err := fn(tx)`でerrをcheckしつつrollback
     - panic時 → deferでpanic時にrecoverしてrollbackする 
- rollbackが必要な一連の流れは、`fn(tx)`で一つの関数としてwrappingする。
```
func WithTx(ctx context.Context, client *ent.Client, fn func(tx *ent.Tx) error) error {
    tx, err := client.Tx(ctx)
    if err != nil {
        return err
    }
    defer func() {
        if v := recover(); v != nil {
            tx.Rollback()
            panic(v)
        }
    }()
    if err := fn(tx); err != nil {
        if rerr := tx.Rollback(); rerr != nil {
            err = fmt.Errorf("%w: rolling back transaction: %v", err, rerr)
        }
        return err
    }
    if err := tx.Commit(); err != nil {
        return fmt.Errorf("committing transaction: %w", err)
    }
    return nil
}
```
- usage
    - ent.Txとent.Clientでは型が違うので、`tx.Client()`でent.Tx → ent.Clientに変換する。
```
func Do(ctx context.Context, client *ent.Client) {
    // WithTx helper.
    if err := WithTx(ctx, client, func(tx *ent.Tx) error {
        return Gen(ctx, tx.Client())
    }); err != nil {
        log.Fatal(err)
    }
}
```

## このprojectの目的
- Q. 2回連続rollbackして問題ないか？
```
1. 通常 → `err := fn(tx)`でerrをcheckしつつrollback
2. panic時 → deferでpanic時にrecoverしてrollbackする
```
- A. 問題なかった
```
root@v0-dev-01:~/project/exercise-golang/tx_sample2# go run main.go 
2024/03/27 06:00:48 USER(id=1, name=alpha)
2024/03/27 06:00:48 USER(id=2, name=bravo)
2024/03/27 06:00:48 USER(id=3, name=charlie)
2024/03/27 06:00:48 [USER(id=1, name=alpha) USER(id=2, name=bravo) USER(id=3, name=charlie)]
2024/03/27 06:00:48 rollback: 1
2024/03/27 06:00:48 rollback: 2
panic: panic test [recovered]
        panic: panic test

goroutine 1 [running]:
main.main.func1()
        /root/project/exercise-golang/tx_sample2/main.go:58 +0xbe
panic({0x98dd80?, 0xaf1fa0?})
        /usr/local/go/src/runtime/panic.go:914 +0x21f
main.main()
        /root/project/exercise-golang/tx_sample2/main.go:70 +0x152
exit status 2
```