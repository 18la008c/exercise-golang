package main

import (
	"context"
	"fmt"
	"log"
	"tx_sample2/ent"

	"entgo.io/ent/dialect"
	_ "github.com/mattn/go-sqlite3"
)

func ConnectDB(ctx context.Context) *ent.Client {
	client, err := ent.Open(dialect.SQLite, "file:ent?mode=memory&cache=shared&_fk=1")
	// client, err := ent.Open(dialect.SQLite, "file:test.db?mode=rw&cache=shared&_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}

	if err := client.Schema.Create(ctx); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	return client
}

func ReadUser(entClient *ent.Client) {
	users, err := entClient.USER.Query().All(context.Background())
	if err != nil {
		log.Fatalf("failed querying users: %v", err)
	}
	log.Println(users)
}

func CreateUser(entClient *ent.Client, name string) {
	user, err := entClient.USER.Create().SetName(name).Save(context.Background())
	if err != nil {
		log.Fatalf("failed creating user: %v", err)
	}
	log.Println(user)
}

func main() {
	ctx := context.Background()
	client := ConnectDB(ctx)

	CreateUser(client, "alpha")
	CreateUser(client, "bravo")
	CreateUser(client, "charlie")

	ReadUser(client)

	tx, _ := client.Tx(ctx)

	defer func() {
		if v := recover(); v != nil {
			_ = tx.Rollback()
			log.Print("rollback: 2")
			panic(v)
		}
	}()

	if err := func(ctx context.Context) error {
		CreateUser(tx.Client(), "delta")  // ent.Tx → ent.Client に変更
		return fmt.Errorf("sample error") // 必ずrollbackするように、tx内でエラーを返す
	}; err != nil {
		log.Print("rollback: 1")
		_ = tx.Rollback() // 本当はrollbackエラーも確認必要だが、今回は省略
	}

	panic("panic test")
}
