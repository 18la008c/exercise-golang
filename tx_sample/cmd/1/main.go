package main

import (
	"context"
	"log"
	"tx_sample/ent"

	"entgo.io/ent/dialect"
	"github.com/google/uuid"

	_ "github.com/mattn/go-sqlite3"
)

const DefaultUUID = "00000000-0000-0000-0000-000000000000"

type BookID uuid.UUID

func (id BookID) String() string {
	return uuid.UUID(id).String()
}

func (id BookID) UUID() uuid.UUID {
	return uuid.UUID(id)
}

func DefaultBookID() BookID {
	return BookID(uuid.MustParse(DefaultUUID))
}

type BookModel struct {
	ID    BookID
	Title string
}

func NewBookModel(title string) BookModel {
	return BookModel{
		ID:    BookID(uuid.New()),
		Title: title,
	}
}

func NewBookModelWithDefaultID(title string) BookModel {
	bookID := DefaultBookID()

	return BookModel{
		ID:    bookID,
		Title: title,
	}
}

func main() {
	client, err := ent.Open(dialect.SQLite, "file:ent?mode=memory&cache=shared&_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
	defer client.Close()

	ctx := context.Background()
	if err := client.Schema.Create(ctx); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	bookModel := NewBookModel("test")
	bookModel2 := NewBookModel("test2")

	bookModelDefault := NewBookModelWithDefaultID("default")
	bookModelDefault2 := NewBookModelWithDefaultID("default2")

	if err := saveTx(ctx, client, bookModel); err != nil {
		log.Printf("saveTx error: %v", err)
	}

	if err := saveTx(ctx, client, bookModel2); err != nil {
		log.Printf("saveTx error: %v", err)
	}

	if err := saveTx(ctx, client, bookModelDefault); err != nil {
		log.Printf("saveTx error: %v", err)
	}

	if err := saveTx(ctx, client, bookModelDefault2); err != nil {
		log.Printf("saveTx error: %v", err)
	}

}

func save(ctx context.Context, client *ent.Client, model BookModel) error {
	err := client.Book.
		Create().
		SetID(model.ID.UUID()).
		SetTitle(model.Title).
		Exec(ctx)
	if err != nil {
		return err
	}

	return nil
}

func saveTx(ctx context.Context, client *ent.Client, model BookModel) error {
	tx, err := client.Tx(ctx)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	err = tx.Book.
		Create().
		SetID(model.ID.UUID()).
		SetTitle(model.Title).
		Exec(ctx)
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}
