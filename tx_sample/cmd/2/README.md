# deferでerrorを扱う場合
- deferだとrollbackが成否に関わらず必ず実行される
- 従って成功した場合にもlogが出て邪魔。。。
```
root@v0-dev-01:~/project/exercise-golang/tx_sample# go run cmd/2/main.go 
2024/01/28 02:48:12 rollback error: %!w(<nil>): sql: transaction has already been committed or rolled back
2024/01/28 02:48:12 rollback error: %!w(<nil>): sql: transaction has already been committed or rolled back
2024/01/28 02:48:12 rollback error: %!w(<nil>): sql: transaction has already been committed or rolled back
2024/01/28 02:48:12 rollback error: ent: constraint failed: UNIQUE constraint failed: books.id
2024/01/28 02:48:12 saveTx error: ent: constraint failed: UNIQUE constraint failed: books.id
```
- deferでlog出力しなければ良いがその場合はerrをdefer内部wrapしてreturnする必要がある。
- deferでは通常defer外部に変数の変更が維持されないので、named return valueを使う必要がある。
- named return valueを使うと、returnが上書きされて元のerrが消えるので、errをwrap(fmt.Errorf("rollback:%s", err))が必要
- 公式はdefer使ってない
- https://entgo.io/ja/docs/transactions/#トランザクションの開始