package books_test

import (
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"

	"ginkgo_sample/books"
)

var _ = Describe("Book", func() {
	var book *books.Book

	BeforeEach(func() {
		book = &books.Book{
			Title:  "Les Miserables",
			Author: "Victor Hugo",
			Pages:  2783,
		}
		Expect(book.IsValid()).To(BeTrue())
	})

	Describe("BookIsvalid", func() {
		Context("without a title", func() {
			It("should not be valid", func() {
				book.Title = ""
				Expect(book.IsValid()).To(BeFalse())
			})
		})
		Context("multiple invalid attr", func() {
			It("should not be valid", func() {
				book.Title = ""
				book.Author = ""
				book.Pages = 0
				Expect(book.IsValid()).To(BeFalse())
			})
		})
	})

	Describe("Categorizing books", func() {
		Context(">300 pages", func() { // contextはcode上の事実を書いて、Itは望ましい結果を書くのが一般的っぽい
			It("should be a novel", func() {
				book.Pages = 301 // この変更はItの中でのみ有効
				Expect(book.Category()).To(Equal(books.CategoryNovel))
			})
		})
		Context("<= 300 pages", func() {
			It("should be a short story", func() {
				book.Pages = 300
				Expect(book.Category()).To(Equal(books.CategoryShortStory))
			})
		})
	})

	Describe("some JSON decoding edge cases", func() {
		var err error

		Context("the JSON fails to parse", func() {
			BeforeEach(func() {
				book, err = books.NewBookFromJSON(`{
					"title":"Les Miserables",
					"author":"Victor Hugo",
					"pages":2783oops
				}`)
			})

			It("returns a nil book", func() {
				Expect(book).To(BeNil())
			})

			It("errors", func() {
				Expect(err).To(MatchError(books.ErrInvalidJSON))
			})
		})
	})
})
