package books

import (
	"encoding/json"
	"errors"
)

const (
	CategoryNovel      = "novel"
	CategoryShortStory = "short story"
)

var (
	ErrInvalidJSON = errors.New("invalid JSON")
)

type Book struct {
	Title  string
	Author string
	Pages  int
}

func NewBookFromJSON(jsonStr string) (*Book, error) {
	book := &Book{}
	if err := json.Unmarshal([]byte(jsonStr), book); err != nil {
		return nil, ErrInvalidJSON
	}
	return book, nil
}

func (b *Book) Category() string {
	if b.Pages > 300 {
		return CategoryNovel
	}
	return CategoryShortStory
}

func (b *Book) IsValid() bool {
	return b.Title != "" && b.Author != "" && b.Pages > 0
}
