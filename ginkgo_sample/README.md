# ginkgo_sample
- https://onsi.github.io/ginkgo/#getting-started
# ginkgoのtestの仕方

## 1. testしたいpkgにcdする
```
root@v0-dev-01:~/project/exercise-golang/ginkgo_sample# cd books
root@v0-dev-01:~/project/exercise-golang/ginkgo_sample/books# tree
.
└── books.go

0 directories, 1 file
```

## 2. test_suiteを作成する
- pkg名に応じたtestの実行scriptが生成される
- 注意: pkgごとにsuite_test.goが必要！！！
```
root@v0-dev-01:~/project/exercise-golang/ginkgo_sample/books# ginkgo bootstrap
Generating ginkgo test suite bootstrap for books in:
        books_suite_test.go
root@v0-dev-01:~/project/exercise-golang/ginkgo_sample/books# tree
.
├── books.go
└── books_suite_test.go
```
- `books_suite_test.go`
```

func TestBooks(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Books Suite")
}
```

## 3. 実際のtest内容を記載するファイルを作成する
- 2で作ったのはtest実行用fileで、実際にtestは記載しない(記載しても良いが複数test fileを作る場合は分けたほうが良いため)
```
root@v0-dev-01:~/project/exercise-golang/ginkgo_sample/books# ginkgo generate books
Generating ginkgo test for Books in:
  books_test.go
root@v0-dev-01:~/project/exercise-golang/ginkgo_sample/books# tree
.
├── books.go
├── books_suite_test.go
└── books_test.go

0 directories, 3 files
```

## 4. 実際にtest codeをbooks.testに記載する。

## 5. testの実行
- `-r`で全directoryを対象
```
root@v0-dev-01:~/project/exercise-golang/ginkgo_sample/books# ginkgo -r
Running Suite: Books Suite - /root/project/exercise-golang/ginkgo_sample/books
==============================================================================
Random Seed: 1714204792

Will run 2 of 2 specs
••

Ran 2 of 2 Specs in 0.000 seconds
SUCCESS! -- 2 Passed | 0 Failed | 0 Pending | 0 Skipped
PASS

Ginkgo ran 1 suite in 329.750184ms
Test Suite Passed
```