package main

import (
	"net/http"

	"api_sample3/controller"
	"api_sample3/model"

	"github.com/labstack/echo/v4"
)

func connect(c echo.Context) error {
	db, _ := model.DB.DB() //memo DB()ってなに？ globalで宣言したんだから、model.DBだけで良くない？
	defer db.Close()

	if err := db.Ping(); err != nil {
		return c.String(http.StatusInternalServerError, "cannot connect db")
	} else {
		return c.String(http.StatusOK, "connect db")
	}
}
func main() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		return c.String(http.StatusOK, "Hello World.")
	})

	e.GET("/ping", connect)

	e.GET("/users/:id", controller.GetUser)

	//curl -X GET localhost:8080/users
	e.GET("/users", controller.GetUsers)

	//curl -X POST -H "Content-Type: application/json" -d '{"name":"ABC"}' localhost:8080/users
	//>>{"id":1,"name":"ABC","created_at":"2023-08-05T11:47:40.689Z","updated_at":"2023-08-05T11:47:40.689Z"}
	e.POST("/users", controller.CreateUser)

	e.PUT("/users/:id", controller.UpdateUser)

	e.Logger.Fatal(e.Start((":8080")))
}
