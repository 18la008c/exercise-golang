package controller

import (
	"api_sample3/model"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
)

func CreateUser(c echo.Context) error {
	user := model.User{}
	if err := c.Bind(&user); err != nil {
		return err //memo errが返ってきたらどうするの？
	}

	model.DB.Create(&user) //DBはmodel内でglobal宣言しているから引数に入れなくても使える。
	return c.JSON(http.StatusCreated, user)
}

func GetUsers(c echo.Context) error {
	//users := model.User{}
	var users []model.User
	model.DB.Find(&users) //memo　usersに代入してないのに、検索結果代入できるとはこれいかに？
	return c.JSON(http.StatusOK, users)

}

func GetUser(c echo.Context) error {
	//user := model.User{}
	var user model.User

	//Bindによって、/user/:idのidが、struct UserのIDに紐づく。これはparam tagとpath-parameterを紐付ける機能があるから
	if err := c.Bind(&user); err != nil {
		return err
	}

	//recodeが無いときはちゃんとerrを補足しないと、default valueのuserが返ってしまう。。。
	if err := model.DB.Take(&user); err != nil {
		return c.JSON(http.StatusBadRequest, map[string]string{"error": "Record not found!"})
	}
	return c.JSON(http.StatusOK, user)
}

func UpdateUser(c echo.Context) error {
	user := model.User{}

	if err := c.Bind(&user); err != nil {
		return err
	}

	if err := model.DB.Save(&user); err != nil { //SaveもCreateも新規作成で、idが存在しなくても実行できるので注意
		log.Print(err)
	}
	return c.JSON(http.StatusOK, user)

}
