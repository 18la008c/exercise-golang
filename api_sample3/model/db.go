package model

import (
	"log"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB //DBは使いまわしたいのでglobalで宣言
var err error   // errorをglobalにする必要はないはず。

// init()は特殊関数で、pkgをimportした時に自動で呼ばれる。
func init() {
	// pydantic.PostgresDsnのようにwrappingしてくれるツールは特にない。
	dsn := "mysql:mysql@tcp(db:3306)/test?charset=utf8mb4&parseTime=True&loc=Local"
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{}) //memo configってなに
	if err != nil {
		log.Fatalln(dsn + "database cannot connect")
	}

	err = DB.AutoMigrate((&User{})) //memo automigrateってなにするの？、　なんで&つけるの？
}
