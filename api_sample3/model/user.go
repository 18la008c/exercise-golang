package model

import "time"

// https://echo.labstack.com/docs/binding#data-sources
type User struct {
	ID        uint      `json:"id" param:"id"` //paramをつけると、path-parameterの値が自動で紐づく。 /user/:idの場合はこのidの値が紐づく
	Name      string    `json:"name" binding:"required"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
