# 目的
- auth_sample2を発展させて、api対応、db対応をする

## create
```
root@v0-dev-01:~/project/exercise-golang/auth_sample3# curl -i -X POST \mple3# curl -i -X POST \
        -d '{"email":"alpha@example.com","password":"AlphaPasswo@rd"}' \
        localhost:8080/token
HTTP/1.1 200 OK
Date: Thu, 28 Dec 2023 08:39:43 GMT
Content-Length: 99
Content-Type: text/plain; charset=utf-8

eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxfQ.jYyRJbb0WImFoUUdcslQQfwnXTHJzne-6tsPd8Hrw0I
```

## Authenticate
```
root@v0-dev-01:~/project/exercise-golang/auth_sample3# curl -i -X GET \
        -H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoxfQ.jYyRJbb0WImFoUUdcslQQfwnXTHJzne-6tsPd8Hrw0I" \
        localhost:8080/admin
HTTP/1.1 200 OK
Date: Thu, 28 Dec 2023 08:25:32 GMT
Content-Length: 24
Content-Type: text/plain; charset=utf-8

root@v0-dev-01:~/project/exercise-golang/auth_sample3# curl -i -X POST \mple3# curl -i -X POST \
        -d '{"email":"alpha@example.com","password":"AlphaPasswo@rd"}' \
        localhost:8080/token
```