package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwt"

	"auth_sample3/ent"
	"auth_sample3/ent/user"

	_ "github.com/mattn/go-sqlite3"
)

const (
	JWTSECRET = "secret"
)

func init() {
	ctx := context.Background()
	entCient, err := NewEntClient(ctx)
	if err != nil {
		log.Fatal(err)
	}

	if err := entCient.User.Create().
		SetEmail("alpha@example.com").
		SetPassword("AlphaPasswo@rd").
		SetName("alpha").
		Exec(ctx); err != nil {
		log.Fatal(err)
	}

	if err := entCient.User.Create().
		SetEmail("bravo@example.com").
		SetPassword("BravoPasswo@rd").
		SetName("bravo").
		Exec(ctx); err != nil {
		log.Fatal(err)
	}

	if err := entCient.User.Create().
		SetEmail("charlie@example.com").
		SetPassword("CharliePasswo@rd").
		SetName("charlie").
		Exec(ctx); err != nil {
		log.Fatal(err)
	}
}

func NewEntClient(ctx context.Context) (*ent.Client, error) {
	entClient, err := ent.Open("sqlite3", "file:ent?mode=memory&cache=shared&_fk=1")
	if err != nil {
		return nil, err
	}
	if err := entClient.Schema.Create(ctx); err != nil {
		return nil, err
	}
	return entClient, nil
}

func main() {
	router, err := createRouter()
	if err != nil {
		log.Fatal(err)
	}

	//listen and serve
	log.Println("listening on port 8080")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func createRouter() (*chi.Mux, error) {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Group(func(r chi.Router) {
		r.Use(Authenticator)
		r.Get("/admin", func(w http.ResponseWriter, r *http.Request) {
			userID := r.Context().Value("userID")
			w.Write([]byte(fmt.Sprintf("protected area. hi admin(user_id:%s)", userID)))
		})
	})

	r.Group(func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			w.Write([]byte("welcome anonymous"))
		})
		r.Post("/token", createToken)
	})

	return r, nil
}

func Authenticator(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Get token from authorization header.
		tokenString := r.Header.Get("Authorization")
		if tokenString == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		tokenString = strings.Replace(tokenString, "Bearer ", "", 1)

		key := jwt.WithKey(jwa.SignatureAlgorithm("HS256"), []byte(JWTSECRET))
		token, err := jwt.ParseString(tokenString, key)
		if err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		if err := jwt.Validate(token); err != nil {
			log.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		userID, ok := token.Get("user_id")
		if !ok {
			log.Println(err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		log.Printf("userID: %+v", userID)
		//TODO: token.GETはinterface{}を返すので、型を確認する必要がある + なぜかfloat64で返ってくるのでstringに変換する必要がある
		ctx := context.WithValue(r.Context(), "userID", strconv.FormatFloat(userID.(float64), 'f', -1, 64))
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

type UserRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func createToken(w http.ResponseWriter, r *http.Request) {
	//parse request body from json
	var u UserRequest
	if err := json.NewDecoder(r.Body).Decode(&u); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	entClient, err := NewEntClient(r.Context())
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//get user from db
	entUser, err := entClient.User.Query().Where(user.Email(u.Email)).Only(r.Context())
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	//check password
	if entUser.Password != u.Password {
		log.Println(err)
		w.WriteHeader(http.StatusForbidden)
		return
	}

	//create token
	token := jwt.New()
	token.Set("user_id", entUser.ID)
	key := jwt.WithKey(jwa.SignatureAlgorithm("HS256"), []byte(JWTSECRET))
	signedToken, err := jwt.Sign(token, key)
	if err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Write([]byte(signedToken))
}
