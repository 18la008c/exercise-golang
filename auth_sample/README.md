```
.
├── cmd
│   ├── chi_jwt
│   │   └── main.go  # chiで認証をするAPIのsample
│   ├── handlerfunc1
│   │   └── main.go # http.Hanlderとhttp.HandlerFuncの違いを検証したcode
│   ├── handlerfunc2
│   │   └── main.go # http.Hanlderとhttp.HandlerFuncの違いを検証したcode2
│   └── middleware1
│       └── main.go # golangでmiddlewareの挙動について確認したcode
├── go.mod
├── go.sum
└── README.md
```