package main

import (
	"fmt"
	"net/http"
	"reflect"
)

func hello(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Hello World from HandlerFunc")
}

func dir(obj interface{}) {
	typeInfo := reflect.TypeOf(obj)
	// メソッドの情報を取得
	for i := 0; i < typeInfo.NumMethod(); i++ {
		method := typeInfo.Method(i)
		fmt.Println("Method:", method.Name)
	}
}

func main() {
	fmt.Printf("%T\n", hello)
	dir(hello)

	fmt.Printf("%T\n", http.HandlerFunc(hello))
	dir(http.HandlerFunc(hello))
}
