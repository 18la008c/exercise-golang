// https://journal.lampetty.net/entry/understanding-http-handler-in-go
package main

import (
	"fmt"
	"log"
	"net/http"
)

// HandlerFunc(=Handler): ただの関数だが、func(w http.ResponseWriter, r *http.Request)と引数が決まっている。(勝手にServeHTTPを実装してくれる = http.Handlerになる)
func hello(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Hello World from HandlerFunc")
}

// Handler: ServeHTTPを実装しているので、http.Handlerになる
type helloHandler struct{}

func (h *helloHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Hello World from Hanlder")
}

func main() {
	mux := http.NewServeMux()
	//HandlerFuncを使う場合
	mux.HandleFunc("/hello", hello)

	//HandlerFunc → Handlerに変換して使う場合
	mux.Handle("/hello1", http.HandlerFunc(hello))

	//Handlerを使う場合
	mux.Handle("/hello2", &helloHandler{})
	log.Fatal(http.ListenAndServe(":3333", mux))
}
