package main

import (
	"fmt"
	"log"
	"net/http"
)

// indexHandler ...
func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Hello, middleware!")
}

// aboutHandler ...
func aboutHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("This is midlleware test!!")
}

// middleware1 ...
func middleware1(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("[START] middleware1")
		next.ServeHTTP(w, r)
		fmt.Println("[END] middleware1")
	}
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", middleware1(indexHandler))
	mux.HandleFunc("/about", middleware1(aboutHandler))

	log.Fatal(http.ListenAndServe(":3333", mux))
}
