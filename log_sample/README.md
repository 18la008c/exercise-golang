.
├── cmd
│   ├── slog
│   │   └── main.go // slogについて実験
│   ├── slog2
│   │   └── main.go // 実験結果後に必要な機能をまとめたもの
│   └── zap
│       └── main.go // zapを実験
├── go.mod
├── go.sum
└── README.md