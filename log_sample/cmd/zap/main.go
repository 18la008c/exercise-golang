package main

import (
	"time"

	"go.uber.org/zap"
)

func main() {
	logger, _ := zap.NewProduction()
	defer logger.Sync() // nolint: errcheck
	sugar := logger.Sugar()
	url := "http://example.com"
	sugar.Infow(
		"failed to fetch URL",
		// key-valueのペアを渡して構造化logを出力
		"url", url,
		"attempt", 3,
		"backoff", time.Second,
	)
	sugar.Infof("Failed to fetch URL: %s", url) //出力はjsonだが、構造化されていないのでkey-valueのペアを渡せない
}

//root@v0-dev-01:~/project/exercise-golang/log_sample# go run cmd/zap/main.go
// {"level":"info","ts":1703945522.3948398,"caller":"zap/main.go:14","msg":"failed to fetch URL","url":"http://example.com","attempt":3,"backoff":1}
// {"level":"info","ts":1703945522.3948843,"caller":"zap/main.go:20","msg":"Failed to fetch URL: http://example.com"}

//sugar := logger.Sugar()
// Zapは、パフォーマンスのためにログメッセージをバッファリングします。つまり、ログメッセージはバッファに一時的に保存され、バッファが一定のサイズに達するか、特定のイベントが発生するまで出力されません。これにより、ログ出力が効率的に行われ、パフォーマンスが向上します。
// しかし、プログラムが終了する前にログバッファがフラッシュされない場合、一部のログメッセージが出力されない可能性があります。これは、予期しない終了やクラッシュが発生した場合に特に問題となります。
