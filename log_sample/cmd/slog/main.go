// refs: https://gihyo.jp/article/2023/02/tukinami-go-04
// refs: https://zenn.dev/88888888_kota/articles/7e97ff874083cf
package main

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"path"
	"strings"
	"time"
)

func SetupLogger() *slog.Logger {
	logger := slog.New(slog.NewJSONHandler(os.Stdout, nil))
	return logger
}

func SetupDebugLogger() *slog.Logger {
	opts := slog.HandlerOptions{
		AddSource: true, // codeの場所を教えてくれるけど、長くて邪魔。ここはzapの方が良い。"source":{"function":"main.main","file":"/root/project/exercise-golang/log_sample/cmd/slog/main.go","line":82}
		Level:     slog.LevelDebug,
	}
	logger := slog.New(slog.NewJSONHandler(os.Stdout, &opts))
	return logger
}

func MaskingLogger() *slog.Logger {
	opts := slog.HandlerOptions{
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {

			if strings.Contains(a.Value.String(), "passw@rd") {
				newStr := strings.ReplaceAll(a.Value.String(), "passw@rd", "********")
				a.Value = slog.StringValue(newStr)
			}
			return a
		},
	}
	logger := slog.New(slog.NewJSONHandler(os.Stdout, &opts))
	return logger
}

func JSTLogger() *slog.Logger {
	opts := slog.HandlerOptions{
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.TimeKey {
				const layout = "2006-01-02 03:04:05"

				t := a.Value.Time()
				jst := time.FixedZone("Asia/Tokyo", 9*60*60)
				a.Value = slog.StringValue(t.In(jst).Format(layout))
			}
			return a
		},
	}
	logger := slog.New(slog.NewJSONHandler(os.Stdout, &opts))
	return logger
}

func SimpleSourceLogger() *slog.Logger {
	opts := slog.HandlerOptions{
		AddSource: true,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.SourceKey {
				s := a.Value.Any().(*slog.Source)
				s.File = path.Base(s.File)
			}
			return a
		},
	}
	logger := slog.New(slog.NewJSONHandler(os.Stdout, &opts))
	return logger
}

func MoreSimpleSourceLogger() *slog.Logger {

	opts := slog.HandlerOptions{
		AddSource: true,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == slog.SourceKey {
				s := a.Value.Any().(*slog.Source)
				s.File = path.Base(s.File)
				a.Value = slog.StringValue(fmt.Sprintf("%s:%d", s.File, s.Line))
			}
			return a
		},
	}
	logger := slog.New(slog.NewJSONHandler(os.Stdout, &opts))

	return logger
}

func main() {
	logger := SetupLogger()

	logger.Debug("Debug message")
	logger.Info("Info message")
	logger.Warn("Warning message")
	logger.Error("Error message")
	// Debugはdefaultでは出力されない
	// {"time":"2023-12-30T14:37:32.49410488Z","level":"INFO","msg":"Info message"}
	// {"time":"2023-12-30T14:37:32.494216927Z","level":"WARN","msg":"Warning message"}
	// {"time":"2023-12-30T14:37:32.494439709Z","level":"ERROR","msg":"Error message"}

	slog.Info("default logger")
	// 2023/12/30 14:37:32 INFO default logger
	//　slogを使うとdefault loggerになる

	logger.Info(
		"structured logger message",
		"id", 1,
		"method", "GET",
		"path", "/v1/users",
		"status", 200,
	)
	// 型指定なしでも出力できる
	// {"time":"2023-12-30T14:37:32.494521248Z","level":"INFO","msg":"structured logger message","id":1,"method":"GET","path":"/v1/users","status":200}

	logger.LogAttrs(
		context.Background(),
		slog.LevelInfo,
		"structured logger message",
		slog.Int("id", 1),
		slog.String("method", "GET"),
		slog.String("path", "/v1/users"),
		slog.Int("status", 200),
	)
	// 型指定した方が安心 & 早い
	// {"time":"2023-12-30T14:37:32.494521248Z","level":"INFO","msg":"structured logger message","id":1,"method":"GET","path":"/v1/users","status":200}

	childLogger := logger.With(
		slog.Int("id", 1),
	)
	childLogger.Info("child logger message")
	childLogger.Error("error message")
	// 子loggerを作成できる。 + 常に同じkey-valueを渡せる
	// {"time":"2023-12-30T14:49:05.317067789Z","level":"INFO","msg":"child logger message","id":1}
	// {"time":"2023-12-30T14:49:05.317072258Z","level":"ERROR","msg":"error message","id":1}

	logger.Info(
		"nest json",
		slog.Int("id", 1),
		slog.Group("nest1",
			slog.Int("key1", 1000),
			slog.Int("key2", 2000),
		),
	)
	// slog.Groupにより、jsonのnestも可能
	// {"time":"2023-12-30T14:51:42.249251087Z","level":"INFO","msg":"nest json","id":1,"nest1":{"key1":1000,"key2":2000}}

	debugLogger := SetupDebugLogger()
	debugLogger.Debug("Debug message")
	// 独自のoption`slog.HandlerOptions`をhandlerに渡すことで、sourceやloglevelを変更できる
	// {"time":"2023-12-30T15:09:26.932364353Z","level":"DEBUG","source":{"function":"main.main","file":"/root/project/exercise-golang/log_sample/cmd/slog/main.go","line":82},"msg":"Debug message"}

	maskingLogger := MaskingLogger()
	maskingLogger.Info("password is passw@rd and passw@rd")
	//　optionのReplaceAttrを変更することで、特定の文字列をマスクすることも可能
	// {"time":"2023-12-30T15:23:17.810366887Z","level":"INFO","msg":"password is ******** and ********"}

	jstLogger := JSTLogger()
	jstLogger.Info("JST time", slog.String("request", "GET"), "URL", "https://example.com")
	// optionのReplaceAttrを使うことで、timeのformatも変更できる
	// {"time":"2023-12-31 02:14:12","level":"INFO","msg":"password is passw@rd and passw@rd"}

	simpleLogger := SimpleSourceLogger()
	simpleLogger.Info("simple source logger")
	// full pathを出力するのは邪魔なので、path.Baseを使ってfile名のみにする
	// {"time":"2024-01-01T08:42:50.389418143Z","level":"INFO","source":{"function":"main.main","file":"main.go","line":168},"msg":"simple source logger"}

	moreSimpleLogger := MoreSimpleSourceLogger()
	moreSimpleLogger.Info("more simple source logger")
	// zapとほぼ同じ形式にする。ただしzapはdir/*.goの形式だが、こちらは*..goのみの出力
	// zapの真似するなら https://github.com/uber-go/zap/blob/d27427d23f81dba1f048d6034d5f286572049e1e/zapcore/entry.go#L100
	// {"time":"2024-01-01T08:42:50.389425307Z","level":"INFO","source":"main.go:171","msg":"more simple source logger"}

}
