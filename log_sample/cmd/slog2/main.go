package main

import (
	"fmt"
	"log/slog"
	"os"
	"path"
	"strings"
	"time"
)

var appEnv = os.Getenv("APP_ENV")

func initOptions(loglevel slog.Level) *slog.HandlerOptions {
	return &slog.HandlerOptions{
		AddSource: true,
		Level:     loglevel,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			switch a.Key {
			case slog.TimeKey:
				const layout = "2006-01-02 03:04:05"
				t := a.Value.Time()
				jst := time.FixedZone("Asia/Tokyo", 9*60*60)
				a.Value = slog.StringValue(t.In(jst).Format(layout))
			case slog.SourceKey:
				s := a.Value.Any().(*slog.Source)
				s.File = path.Base(s.File)
				a.Value = slog.StringValue(fmt.Sprintf("%s:%d", s.File, s.Line))
			}
			if strings.Contains(a.Value.String(), "passw@rd") {
				newStr := strings.ReplaceAll(a.Value.String(), "passw@rd", "********")
				a.Value = slog.StringValue(newStr)
			}
			return a
		},
	}
}

func NewLogger() *slog.Logger {
	// refs: https://betterstack.com/community/guides/logging/logging-in-go/#customizing-the-default-logger
	if appEnv == "production" || appEnv == "prod" {
		handler := slog.NewJSONHandler(os.Stdout, initOptions(slog.LevelInfo))
		return slog.New(handler)

	} else {
		handler := slog.NewTextHandler(os.Stdout, initOptions(slog.LevelDebug))
		return slog.New(handler)
	}
}

func main() {
	logger := NewLogger()
	logger.Info("sample log", slog.String("password", "passw@rd"))
	// {"time":"2024-01-01 06:08:28","level":"INFO","source":"main.go:41","msg":"sample log","password":"********"}
	// > 時刻をJSTに変換、sourceを簡潔出力、passwordをマスク

	err := fmt.Errorf("sample error")
	logger.Error("something went wrong", slog.Any("err", err))
	// errの時は、slog.Anyを使う。 また構造体logの場合はformatterを使わずに出力する
	// https://github.com/golang/go/issues/59364#issuecomment-1493237877
	// https://groups.google.com/g/golang-nuts/c/gUa9nUgVXlk
	// {"time":"2024-01-01 06:08:28","level":"ERROR","source":"main.go:46","msg":"something went wrong","err":"sample error"}

	//もっと出力を綺麗にしたいなら
	//https://zenn.dev/mizutani/articles/golang-clog-handler
}
