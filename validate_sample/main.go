package main

import (
	"log"

	"github.com/go-playground/validator/v10"
)

type Simple struct {
	//変数を大文字(export)しないと、validateできないので注意
	Name string `validate:"required"`
	Age  uint8  `validate:"gte=1,lte=130"`
}

var validate *validator.Validate

func main() {
	validate = validator.New(validator.WithRequiredStructEnabled())

	sOK := &Simple{
		Name: "hiroki",
	}
	log.Print(sOK)

	err := validate.Struct(sOK)
	if err != nil {
		log.Print(err)
	} else {
		log.Print("validate is OK!!!")
	}

	sErr := &Simple{}
	err = validate.Struct(sErr)
	log.Print(sErr)

	if err != nil {
		log.Print(err)
	} else {
		log.Print("validate is OK!!!")
	}
}
