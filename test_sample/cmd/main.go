package main

import (
	"context"
	"fmt"
	"log"
	"test_sample/internal/config"
	"test_sample/internal/domain/user"
	"test_sample/internal/utils/database"
)

func main() {
	cfg := config.New()
	log.Printf("%+v", cfg) //&{Database:0xc0000a4180}とsturctの中身もpointerの時どうやってprintするか分からん

	ctx := context.Background()
	entClient, err := database.NewEntClient(cfg.Database)
	if err != nil {
		log.Fatalf("Faild to connect db: %v", err)
	}

	// auto migrate database by /ent/schema
	if err := entClient.Schema.Create(ctx); err != nil {
		log.Fatalf("Failed creating schema resources: %v", err)
	}

	userRepo := user.NewRepository(entClient)

	//1生成ごとにerrチェックは超面倒では？？？
	u1, err := userRepo.Create(ctx, "alpha", 10)
	if err != nil {
		log.Fatalf("Failed creating user: %v", err)
	}
	u2, err := userRepo.Create(ctx, "bravo", 11)
	if err != nil {
		log.Fatalf("Failed creating user: %v", err)
	}
	u3, err := userRepo.Create(ctx, "charlie", 12)
	if err != nil {
		log.Fatalf("Failed creating user: %v", err)
	}
	fmt.Print(u1, u2, u3)
}
