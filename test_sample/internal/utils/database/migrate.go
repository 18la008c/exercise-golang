package database

import (
	"context"
	"log"
	"test_sample/ent"
)

func Migrate(ctx context.Context, entClient *ent.Client) {
	// auto migrate database by /ent/schema
	if err := entClient.Schema.Create(ctx); err != nil {
		log.Fatalf("Failed creating schema resources: %v", err)
	}
	log.Println("Database migration successful.")
}
