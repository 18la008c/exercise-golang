package user

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"test_sample/ent"
	"test_sample/internal/config"
	"test_sample/internal/utils/database"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/ory/dockertest/v3"
	"gotest.tools/v3/assert"
)

var (
	entClient *ent.Client
	cfg       *config.Database = &config.Database{
		Driver:   "mysql",
		Host:     "localhost",
		Port:     3306,
		Name:     "test",
		User:     "root",
		Password: "dockertest", //composeのDBに間違ってアクセスしないように、passwordを分ける。
	}
)

// TODO: packets.go:37: unexpected EOFの解消 → NewEntClientを使うと見えなくなるので注意。
func TestMain(m *testing.M) {

	// 1. dockerと接続。空文字だとDOCKER_HOST等の環境変数から自動取得する。
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Panicf("Could not construct pool: %s", err)
	}
	pool.MaxWait = time.Minute * 1

	// 2. dockerdに疎通チェック
	if err = pool.Client.Ping(); err != nil {
		log.Panicf("Could not connect to Docker: %s", err)
	}

	// 3. containerの作成
	resource, err := pool.Run("mysql", "8", []string{
		fmt.Sprintf("MYSQL_ROOT_PASSWORD=%s", cfg.Password),
		fmt.Sprintf("MYSQL_TCP_PORT=%d", cfg.Port),
		fmt.Sprintf("MYSQL_DATABASE=%s", cfg.Name),
	})
	if err != nil {
		log.Panicf("Could not start resource: %s", err)
	}
	defer closeContainer(resource, pool)

	// 4. GetPortで3306とrandomでbindされたport番号取得する。
	port, err := strconv.ParseUint(resource.GetPort("3306/tcp"), 10, 16)
	if err != nil {
		log.Fatalln(err)
	}
	cfg := &config.Database{
		Driver:   "mysql",
		Host:     "localhost",
		Port:     uint16(port),
		Name:     "test",
		User:     "root",
		Password: "dockertest", //composeのDBに間違ってアクセスしないように、passwordを分ける。
	}
	// 5. RetryでcontainerのDBが起動するのを待つ。
	if err := pool.Retry(
		func() error {
			// `:=`でentClientを再定義すると、package global変数じゃなくなるので注意
			entClient, err = database.NewEntClient(cfg)
			return err
		},
	); err != nil {
		log.Panicf("Could not connect to database: %s", err)
	}
	defer closeEntClient(entClient)

	ctx := context.Background()
	database.Migrate(ctx, entClient)

	// 6. Before script終了
	m.Run()
	// 7. After script開始

}
func closeEntClient(entClient *ent.Client) {
	if err := entClient.Close(); err != nil {
		log.Panicf("Could not close ent client:%s", err)
	}
}

func closeContainer(resource *dockertest.Resource, pool *dockertest.Pool) {
	if err := pool.Purge(resource); err != nil {
		log.Panicf("Could not purge resource: %s", err)
	}
}

func TestRepository_Create(t *testing.T) {
	repo := NewRepository(entClient)
	ctx := context.Background()

	//Create(ctx context.Context, name string, age uint8) 255はdefault typeがintだが、引数でage uint8としているので自動で型変換される
	created, err := repo.Create(ctx, "alpha", 255)

	assert.Equal(t, err, nil)
	assert.Equal(t, "alpha", created.Name)

	//assert.Equal(t assert.TestingT, x interface{}, y interface{},...)と inteface{}なので、255のdefault typeであるintとなる。従って手動でuint8(255)の型変換が必要
	assert.Equal(t, uint8(255), created.Age)
}

func TestRepository_Read(t *testing.T) {
	repo := NewRepository(entClient)
	ctx := context.Background()

	read, err := repo.Read(ctx, "alpha")
	assert.Equal(t, err, nil)
	assert.Equal(t, "alpha", read.Name)
	assert.Equal(t, uint8(255), read.Age)
}

func TestRepository_Update(t *testing.T) {
	//Updateの単体テストができない。。。 updateのためにent.Carという別のtableのschemaが必要
	//func (r *userRepository) Update(ctx context.Context, user *ent.User, cars ...*ent.Car)
}
