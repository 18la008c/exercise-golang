package config

import (
	"log"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	Database *Database
}

type Database struct {
	Driver   string
	Host     string
	Port     uint16
	Name     string
	User     string
	Password string
}

func newDatabase() *Database {
	var db Database
	envconfig.MustProcess("DB", &db)

	return &db
}

func New() *Config {
	err := godotenv.Load()
	if err != nil {
		log.Println(err)
	}

	return &Config{
		Database: newDatabase(),
	}
}
