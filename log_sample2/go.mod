module log_sample2

go 1.21.1

require (
	github.com/go-chi/chi/v5 v5.0.11
	github.com/go-chi/httplog/v2 v2.0.8
	github.com/google/uuid v1.5.0
)
