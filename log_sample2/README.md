# result
-  system logger
```
{
  "timestamp": "2024-01-04T15:25:34Z",
  "severity": "INFO",
  "source": {
    "function": "main.main",
    "file": "/root/project/exercise-golang/log_sample2/main.go",
    "line": 38
  },
  "message": "Starting httplog-example",
  "service": "httplog-example",
  "tags": {
    "version": "v1.0-81aa4244d9fc8076a",
    "env": "dev"
  }
}
```
- request log (auto created by middleware)
```
{
  "timestamp": "2024-01-04T15:25:38Z",
  "severity": "INFO",
  "source": {
    "function": "github.com/go-chi/httplog/v2.(*requestLogger).NewLogEntry",
    "file": "/root/go/pkg/mod/github.com/go-chi/httplog/v2@v2.0.8/httplog.go",
    "line": 120
  },
  "message": "Request: GET /info",
  "service": "httplog-example",
  "tags": {
    "version": "v1.0-81aa4244d9fc8076a",
    "env": "dev"
  },
  "httpRequest": {
    "url": "http://localhost:8000/info",
    "method": "GET",
    "path": "/info",
    "remoteIP": "127.0.0.1:48278",
    "proto": "HTTP/1.1",
    "requestID": "v0-dev-01/srzaBsoYrQ-000001",
    "scheme": "http",
    "header": {
      "user-agent": "curl/7.81.0",
      "accept": "*/*"
    }
  }
}
```
- access logger (by oplog.Info("info here"))
```
{
  "timestamp": "2024-01-04T15:25:38Z",
  "severity": "INFO",
  "source": {
    "function": "main.main.func5",
    "file": "/root/project/exercise-golang/log_sample2/main.go",
    "line": 79
  },
  "message": "info here",
  "service": "httplog-example",
  "tags": {
    "version": "v1.0-81aa4244d9fc8076a",
    "env": "dev"
  },
  "httpRequest": {
    "url": "http://localhost:8000/info",
    "method": "GET",
    "path": "/info",
    "remoteIP": "127.0.0.1:48278",
    "proto": "HTTP/1.1",
    "requestID": "v0-dev-01/srzaBsoYrQ-000001",
    "scheme": "http",
    "header": {
      "user-agent": "curl/7.81.0",
      "accept": "*/*"
    }
  },
  "user": "user",
  "account": "3144fb60-81ff-43b6-af92-28c680485832"
}
```

- response logger (auto created by middleware)
```
{
  "timestamp": "2024-01-04T15:25:38Z",
  "severity": "INFO",
  "source": {
    "function": "github.com/go-chi/httplog/v2.(*RequestLoggerEntry).Write",
    "file": "/root/go/pkg/mod/github.com/go-chi/httplog/v2@v2.0.8/httplog.go",
    "line": 155
  },
  "message": "Response: 200 OK",
  "service": "httplog-example",
  "tags": {
    "version": "v1.0-81aa4244d9fc8076a",
    "env": "dev"
  },
  "httpRequest": {
    "url": "http://localhost:8000/info",
    "method": "GET",
    "path": "/info",
    "remoteIP": "127.0.0.1:48278",
    "proto": "HTTP/1.1",
    "requestID": "v0-dev-01/srzaBsoYrQ-000001",
    "scheme": "http",
    "header": {
      "user-agent": "curl/7.81.0",
      "accept": "*/*"
    }
  },
  "user": "user",
  "account": "3144fb60-81ff-43b6-af92-28c680485832",
  "httpResponse": {
    "status": 200,
    "bytes": 9,
    "elapsed": 0.033756
  }
}
```

## defaultのslogを使った場合
- httplog
```
{"timestamp":"2024-01-05T02:14:05Z","severity":"INFO","source":{"function":"main.main","file":"/root/project/exercise-golang/log_sample2/main.go","line":38},"message":"Starting httplog-example","service":"httplog-example","tags":{"version":"v1.0-81aa4244d9fc8076a","env":"dev"}}
```
- slog(default)
    - httplog内部でSetDefaultしてると思ったけど変わってない
```
{"timestamp":"2024-01-05T02:14:05Z","severity":"INFO","source":{"function":"main.main","file":"/root/project/exercise-golang/log_sample2/main.go","line":39},"message":"log test by slog default logger"}
```