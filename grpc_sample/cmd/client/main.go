package main

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"os"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	hellopb "grpc/pkg/grpc"
)

func Hello(s *bufio.Scanner, c hellopb.GreetingServiceClient) {
	fmt.Println("Please enter your name.")
	s.Scan()

	name := s.Text()

	req := &hellopb.HelloRequest{
		Name: name,
	}
	res, err := c.Hello(context.Background(), req)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res.GetMessage())
	}
}

func HelloServerStream(s *bufio.Scanner, c hellopb.GreetingServiceClient) {
	fmt.Println("Please enter your name.")
	s.Scan()
	name := s.Text()

	req := &hellopb.HelloRequest{
		Name: name,
	}
	stream, err := c.HelloServerStream(context.Background(), req)
	if err != nil {
		fmt.Println(err)
		return
	}

	for {
		res, err := stream.Recv()
		if errors.Is(err, io.EOF) {
			fmt.Println("all the responses have already received.")
			break
		}

		if err != nil {
			fmt.Println(err)
		}
		fmt.Println(res)
	}
}

func HelloClientStream(s *bufio.Scanner, c hellopb.GreetingServiceClient) {
	stream, err := c.HelloClientStream(context.Background())
	if err != nil {
		fmt.Println(err)
		return
	}

	sendCount := 5
	fmt.Printf("Please enter %d names.\n", sendCount)

	for i := 0; i < sendCount; i++ {
		s.Scan()
		name := s.Text()

		if err := stream.Send(&hellopb.HelloRequest{Name: name}); err != nil {
			fmt.Println(err)
			return
		}
	}
	res, err := stream.CloseAndRecv()
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(res.GetMessage())
	}
}

func main() {
	fmt.Println("start gRPC Client.")

	scanner := bufio.NewScanner(os.Stdin)

	address := "localhost:8080"

	conn, err := grpc.Dial(
		address,

		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock(),
	)
	if err != nil {
		log.Fatal("Connection failed.")
		return
	}
	defer conn.Close()

	client := hellopb.NewGreetingServiceClient(conn)

	for {
		fmt.Println("1: send Request")
		fmt.Println("2: HelloServerStream")
		fmt.Println("3: HelloClientStream")
		fmt.Println("4: exit")
		fmt.Print("please enter >")

		scanner.Scan()
		input := scanner.Text()

		switch input {
		case "1":
			Hello(scanner, client)
		case "2":
			HelloServerStream(scanner, client)
		case "3":
			HelloClientStream(scanner, client)
		case "4":
			fmt.Println("bye.")
			goto M
		}
	}
M:
}
