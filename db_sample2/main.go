package main

import (
	"context"
	"db_sample2/ent"
	"db_sample2/ent/car"
	"db_sample2/ent/group"
	"db_sample2/ent/user"
	"fmt"
	"log"

	"entgo.io/ent/dialect"
	_ "github.com/mattn/go-sqlite3"
)

func main() {
	client, err := ent.Open(dialect.SQLite, "file:ent?mode=memory&cache=shared&_fk=1")
	// client, err := ent.Open(dialect.SQLite, "file:test.db?mode=rw&cache=shared&_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
	defer client.Close()

	ctx := context.Background()
	if err := client.Schema.Create(ctx); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	// Create user test
	u1, err := CreateUser(ctx, client, "alpha", 10)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Create user test: %v\n", u1)

	// Read user test
	u1, err = QueryUser(ctx, client, "alpha")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Read user test: %v\n", u1)

	// Read user test with some condition
	CreateUser(ctx, client, "bravo", 100)
	CreateUser(ctx, client, "charlie", 21)
	users, err := FilterUserByAge(ctx, client, 20)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Read users test with some condition: %v\n", users)

	// Update user with some cars
	c1, _ := CreateCar(ctx, client, "tesla")
	c2, _ := CreateCar(ctx, client, "ford")
	u1, err = RegisterCarUser(ctx, u1, c1, c2)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Update user with some cars: %v\n", u1)

	// Read cars which has specific user
	cars, err := QueryAllCars(ctx, u1)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Read cars which has specific user: %v", cars)

	// Read user which has specific car
	user, err := QueryCarUsers(ctx, c1)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Read user which has specific car: %v", user)

	CreateGraph(ctx, client)
}

func CreateGraph(
	ctx context.Context,
	client *ent.Client,
) error {
	a8m, _ := CreateUser(ctx, client, "Ariel", 30)
	neta, _ := CreateUser(ctx, client, "Neta", 28)
	bbb, _ := CreateUser(ctx, client, "bbb", 100)

	car1, _ := CreateCar(ctx, client, "Tesla")
	car2, _ := CreateCar(ctx, client, "Mazda")
	car3, _ := CreateCar(ctx, client, "Ford")

	RegisterCarUser(ctx, a8m, car1)
	RegisterCarUser(ctx, a8m, car2)
	RegisterCarUser(ctx, neta, car3)

	group1, _ := CreateGroup(ctx, client, "GitLab")
	group2, _ := CreateGroup(ctx, client, "GitHub")

	AddUsers(ctx, group1, a8m, neta, bbb)
	AddUsers(ctx, group2, a8m)

	//github groupが持つcar一覧を出力
	cars, err := client.Group.
		Query().
		Where(group.Name("GitHub")).
		QueryUsers().
		QueryCars().
		All(ctx)
	if err != nil {
		return fmt.Errorf("failed getting cars: %w", err)
	}
	log.Printf("cars returned: %v\n", cars)

	//車をもつ & 名前がArielのuserを取得
	a8m = client.User.
		Query().
		Where(user.HasCars(), user.Name("Ariel")).
		OnlyX(ctx)

	cars, err = a8m.
		QueryGroups(). //所属してるgroupを全て取得
		QueryUsers().  //上記groupに所属してるuserを全て取得
		QueryCars().   //上記userに所属しているcarを全て取得
		Where(car.Not(car.Model("Mazda"))).
		All(ctx)

	if err != nil {
		return fmt.Errorf("failed getting cars: %w", err)
	}
	log.Println("cars returned:", cars)
	return nil
}

func CreateUser(
	ctx context.Context,
	client *ent.Client,
	name string,
	age int,
) (*ent.User, error) {
	u, err := client.User.Create().
		SetAge(age).
		SetName(name).
		Save(ctx)

	if err != nil {
		return nil, fmt.Errorf("faild creating user: %v", err)
	}
	log.Printf("user was created: %v", u)
	return u, nil
}

func QueryUser(
	ctx context.Context,
	client *ent.Client,
	name string,
) (*ent.User, error) {
	u, err := client.User.Query().Where(user.Name(name)).Only(ctx)
	if err != nil {
		return nil, fmt.Errorf("faild read a user: %v", err)
	}
	return u, nil
}

func FilterUserByAge(
	ctx context.Context,
	client *ent.Client,
	age int,
) ([]*ent.User, error) {
	u, err := client.User.Query().Where(user.AgeGTE(age)).All(ctx)
	if err != nil {
		return nil, fmt.Errorf("faild read a user: %v", err)
	}
	return u, nil
}

func CreateCar(
	ctx context.Context,
	client *ent.Client,
	model string,
) (*ent.Car, error) {
	c, err := client.Car.Create().SetModel(model).Save(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed creating car: %v", err)
	}
	return c, nil
}

func RegisterCarUser(
	ctx context.Context,
	user *ent.User,
	cars ...*ent.Car,
) (*ent.User, error) {
	if err := user.Update().AddCars(cars...).Exec(ctx); err != nil {
		return nil, fmt.Errorf("failed update user: %v", err)
	}
	return user, nil
}

func QueryAllCars(
	ctx context.Context,
	user *ent.User,
) ([]*ent.Car, error) {
	cars, err := user.QueryCars().All(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed querying user cars: %w", err)
	}
	return cars, nil
}

func QueryCarUsers(
	ctx context.Context,
	car *ent.Car,
) (*ent.User, error) {
	user, err := car.QueryOwner().Only(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed querying car %q owner: %w", car.Model, err)
	}
	return user, nil
}

func CreateGroup(
	ctx context.Context,
	client *ent.Client,
	name string,
) (*ent.Group, error) {
	group, err := client.Group.Create().SetName(name).Save(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed creating group: %v", err)
	}
	return group, nil

}

func AddUsers(
	ctx context.Context,
	group *ent.Group,
	users ...*ent.User,
) (*ent.Group, error) {
	if err := group.Update().AddUsers(users...).Exec(ctx); err != nil {
		return nil, fmt.Errorf("failed update group: %v", err)
	}
	return group, nil
}
