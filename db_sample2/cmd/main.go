package main

import (
	"fmt"
)

func Sum(n ...int) int {
	sum := 0
	fmt.Println(n)
	for _, x := range n {
		sum += x
	}
	return sum
}

func main() {
	x := []int{1, 2, 3, 4, 5}
	sum := Sum(x)
	// sum := Sum(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
	fmt.Println(sum)
}
