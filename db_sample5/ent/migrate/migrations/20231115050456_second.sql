-- Modify "users" table
ALTER TABLE `users` ADD COLUMN `age` tinyint unsigned NOT NULL, ADD COLUMN `name` varchar(255) NOT NULL DEFAULT "unknown";
