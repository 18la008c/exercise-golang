package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"

	messagepb "grpc_sample5/messages"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"google.golang.org/protobuf/types/known/structpb"
)

type myServer struct {
	messagepb.UnimplementedUserServiceServer
}

func (s *myServer) SendJson(
	ctx context.Context,
	req *messagepb.SendJsonRequest,
) (*messagepb.SendJsonResponse, error) {
	id := req.GetUserID()
	json, err := createJson(id)
	if err != nil {
		log.Fatal(err)
	}
	return &messagepb.SendJsonResponse{
		Result: json,
	}, nil
}

// func createJson(id string) *structpb.Struct {
// 	return &structpb.Struct{
// 		Fields: map[string]*structpb.Value{
// 			"key1": &structpb.Value{
// 				Kind: &structpb.Value_StringValue{
// 					StringValue: "value1",
// 				},
// 			},
// 			"key2": &structpb.Value{
// 				Kind: &structpb.Value_NumberValue{
// 					NumberValue: 2,
// 				},
// 			},
// 			"id": &structpb.Value{
// 				Kind: &structpb.Value_StringValue{
// 					StringValue: id,
// 				},
// 			},
// 		},
// 	}
// }

// https://pkg.go.dev/google.golang.org/protobuf/types/known/structpb
func createJson(id string) (*structpb.Struct, error) {
	return structpb.NewStruct(map[string]interface{}{
		"key1": "value1",
		"key2": 2,
		"id":   id,
	})
}

func NewMyServer() *myServer {
	return &myServer{}
}

func main() {
	port := 8080
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		panic(err)
	}

	s := grpc.NewServer()

	messagepb.RegisterUserServiceServer(s, NewMyServer())
	reflection.Register(s)

	go func() {
		log.Printf("start gRPC server port: %v", port)
		s.Serve(listener)
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("stopping gRPC server...")
	s.GracefulStop()
}
