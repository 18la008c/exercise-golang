# gen
```
protoc -I messages \
  --go_out=messages \
  --go_opt=paths=source_relative \
  --go-grpc_out=messages \
  --go-grpc_opt=paths=source_relative \
  messages/*.proto
```
-　`-I`があるとprotoと同階層にgenerate
```
├── messages
│   ├── message_grpc.pb.go
│   ├── message.pb.go
│   └── message.proto
└── README.md
```
- ない場合はmessages配下にdirを作って生成される。 `option go_package`の値によるが

```
├── messages
│   ├── message.proto
│   └── messages
│       ├── message_grpc.pb.go
│       └── message.pb.go
└── README.md

```


# 目的
- grpcでjsonを扱う方法
- https://pkg.go.dev/google.golang.org/protobuf/types/known/structpb
```
grpcurl -plaintext -d '{"UserID":"10"}' localhost:8080  messages.UserService.SendJson
{
  "Result": {
      "id": "10",
      "key1": "value1",
      "key2": 2
    }
}
```