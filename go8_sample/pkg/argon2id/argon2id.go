package argon2id

import (
	"crypto/rand"
	"crypto/subtle"
	"encoding/base64"
	"errors"
	"fmt"
	"runtime"
	"strings"

	"golang.org/x/crypto/argon2"
)

// https://datatracker.ietf.org/doc/html/rfc9106#name-recommendations
var (
	ErrInvalidHash         = errors.New("argon2id: hash is not in the correct format")
	ErrMismatchedPassword  = errors.New("argon2id: password does not match hash")
	ErrIncompatibleVariant = errors.New("argon2id: incompatible variant of argon2")
	ErrIncompatibleVersion = errors.New("argon2id: incompatible version of argon2")

	RFC_9106_HIGH_MEMORY = &Params{
		Time:    1,
		Memory:  2 * 1024 * 1024, //2GB
		Threads: uint8(runtime.NumCPU()),
		KeyLen:  32,
		SaltLen: 16,
	}
	RFC_9106_LOW_MEMORY = &Params{
		Time:    3,
		Memory:  64 * 1024, //64MB
		Threads: uint8(runtime.NumCPU()),
		KeyLen:  32,
		SaltLen: 16,
	}
)

type Params struct {
	Time    uint32
	Memory  uint32 //KB
	Threads uint8
	KeyLen  uint32
	SaltLen uint32
}

func GenerateHash(password string, params *Params) (string, error) {
	salt := make([]byte, params.SaltLen)
	if _, err := rand.Read(salt); err != nil {
		return "", err
	}
	key := argon2.IDKey([]byte(password), salt, params.Time, params.Memory, params.Threads, params.KeyLen)
	return fmt.Sprintf(
		"$argon2id$v=%d$m=%d,t=%d,p=%d$%s$%s",
		argon2.Version,
		params.Memory,
		params.Time,
		params.Threads,
		base64.RawStdEncoding.EncodeToString(salt),
		base64.RawStdEncoding.EncodeToString(key),
	), nil
}

func ComparePasswordAndHash(password, hash string) (match bool, err error) {
	params, salt, key, err := decodeHash(hash)
	if err != nil {
		return false, err
	}
	otherKey := argon2.IDKey([]byte(password), salt, params.Time, params.Memory, params.Threads, params.KeyLen)

	if subtle.ConstantTimeEq(int32(len(key)), int32(len(otherKey))) == 0 {
		return false, ErrMismatchedPassword
	}
	if subtle.ConstantTimeCompare(key, otherKey) == 1 {
		return true, nil
	}
	return false, ErrMismatchedPassword
}

func decodeHash(hash string) (params *Params, salt, key []byte, err error) {
	vals := strings.Split(hash, "$")
	if len(vals) != 6 {
		return nil, nil, nil, ErrInvalidHash
	}
	if vals[1] != "argon2id" {
		return nil, nil, nil, ErrIncompatibleVariant
	}
	if vals[2] != fmt.Sprintf("v=%d", argon2.Version) {
		return nil, nil, nil, ErrIncompatibleVersion
	}

	params = &Params{}
	fmt.Sscanf(vals[3], "m=%d,t=%d,p=%d", &params.Memory, &params.Time, &params.Threads)

	salt, err = base64.RawStdEncoding.DecodeString(vals[4])
	if err != nil {
		return nil, nil, nil, err
	}
	params.SaltLen = uint32(len(salt))

	key, err = base64.RawStdEncoding.DecodeString(vals[5])
	if err != nil {
		return nil, nil, nil, err
	}
	params.KeyLen = uint32(len(key))

	return params, salt, key, nil
}
