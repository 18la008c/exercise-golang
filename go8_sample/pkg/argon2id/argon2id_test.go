package argon2id

import (
	"regexp"
	"strings"
	"testing"
)

func TestGenerateHash(t *testing.T) {
	hashRX, err := regexp.Compile(`^\$argon2id\$v=19\$m=65536,t=3,p=[0-9]{1,4}\$[A-Za-z0-9+/]{22}\$[A-Za-z0-9+/]{43}$`)
	if err != nil {
		t.Fatal(err)
	}

	hash1, err := GenerateHash("pas$w@rd", RFC_9106_LOW_MEMORY)
	if err != nil {
		t.Fatal(err)
	}
	if !hashRX.MatchString(hash1) {
		t.Fatal("hash1 is not match")
	}

	hash2, err := GenerateHash("pas$w@rd", RFC_9106_LOW_MEMORY)
	if err != nil {
		t.Fatal(err)
	}
	if !hashRX.MatchString(hash2) {
		t.Fatal("hash2 is not match")
	}

	if strings.Compare(hash1, hash2) == 0 {
		t.Fatal("hash1 and hash2 is same, hash must be unique")
	}
}

func TestComparePasswordAndHash(t *testing.T) {
	hash, err := GenerateHash("pas$w@rd", RFC_9106_LOW_MEMORY)
	if err != nil {
		t.Fatal(err)
	}

	match, err := ComparePasswordAndHash("pas$w@rd", hash)
	if err != nil {
		t.Fatal(err)
	}
	if !match {
		t.Fatal("password and hash is not match")
	}

}
func TestFailedComparePasswordAndHash(t *testing.T) {
	hash, err := GenerateHash("pas$w@rd", RFC_9106_LOW_MEMORY)
	if err != nil {
		t.Fatal(err)
	}
	match, err := ComparePasswordAndHash("otherPas$w@rd", hash)

	if err != ErrMismatchedPassword {
		t.Fatal("err is not ErrMismatchedPassword")
	}
	if match {
		t.Error("expected password and hash to not match")
	}
}

func TestDecodeHash(t *testing.T) {
	hash, err := GenerateHash("pas$w@rd", RFC_9106_LOW_MEMORY)
	if err != nil {
		t.Fatal(err)
	}

	params, _, _, err := decodeHash(hash)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("%+v", params)
	t.Logf("%+v", RFC_9106_LOW_MEMORY)

	if params.Time != RFC_9106_LOW_MEMORY.Time {
		t.Error("params.Time is not RFC_9106_LOW_MEMORY.Time")
	}
	if params.Memory != RFC_9106_LOW_MEMORY.Memory {
		t.Error("params.Memory is not RFC_9106_LOW_MEMORY.Memory")
	}
	if params.Threads != RFC_9106_LOW_MEMORY.Threads {
		t.Error("params.Threads is not RFC_9106_LOW_MEMORY.Threads")
	}
	if params.KeyLen != RFC_9106_LOW_MEMORY.KeyLen {
		t.Error("params.KeyLen is not RFC_9106_LOW_MEMORY.KeyLen")
	}
	if params.SaltLen != RFC_9106_LOW_MEMORY.SaltLen {
		t.Error("params.SaltLen is not RFC_9106_LOW_MEMORY.SaltLen")
	}

}

func TestFailedDecodeHash(t *testing.T) {
	hash := "$argon2id$v=19$m=65536,t=1,p=2$ABC"
	if _, _, _, err := decodeHash(hash); err != ErrInvalidHash {
		t.Fatal("err is not ErrInvalidHash")
	}

	hash = "$6$v=19$m=65536,t=1,p=2$AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA$AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
	if _, _, _, err := decodeHash(hash); err != ErrIncompatibleVariant {
		t.Fatal("err is not ErrIncompatibleVariant")
	}

	hash = "$argon2id$v=219$m=65536,t=1,p=2$AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA$AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
	if _, _, _, err := decodeHash(hash); err != ErrIncompatibleVersion {
		t.Fatal("err is not ErrIncompatibleVersion")
	}
}
