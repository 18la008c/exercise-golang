package config

import (
	"log"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type Config struct {
	API      *API
	Database *Database
}

type API struct {
	Host     string
	Port     uint16
	Password string
}

type Database struct {
	Driver   string
	Host     string
	Port     uint16
	Name     string
	User     string
	Password string
}

func New() *Config {
	err := godotenv.Load()
	if err != nil {
		log.Println(err)
	}

	return &Config{
		API:      newAPI(),
		Database: newDatabase(),
	}
}

func newAPI() *API {
	var api API
	envconfig.MustProcess("API", &api)
	return &api
}

func newDatabase() *Database {
	var db Database
	envconfig.MustProcess("DB", &db)

	return &db
}
