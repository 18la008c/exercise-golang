package binding

import (
	"encoding/json"
	"io"

	"github.com/go-playground/validator/v10"
)

var validate *validator.Validate = validator.New(validator.WithRequiredStructEnabled())

func BindJSON(data io.Reader, obj any) error {
	if err := json.NewDecoder(data).Decode(obj); err != nil {
		return err
	}

	if err := validate.Struct(obj); err != nil { //TODO: これだとvaldiateはpostbody → structにしか使えず、DO等を今後作ったときに使えないので注意
		return err
	}
	return nil
}
