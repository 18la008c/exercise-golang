package database

import (
	"context"

	"go8_sample/ent"
)

func Migrate(ctx context.Context, entClient *ent.Client) error {
	// auto migrate database by /ent/schema
	if err := entClient.Schema.Create(ctx); err != nil {
		return err
	}
	return nil
}
