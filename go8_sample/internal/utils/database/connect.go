package database

import (
	"fmt"
	"time"

	"go8_sample/ent"
	"go8_sample/internal/config"

	entsql "entgo.io/ent/dialect/sql"
	"github.com/go-sql-driver/mysql"
)

// https://entgo.io/docs/sql-integration/#configure-sqldb
// https://github.com/ent/ent/issues/661
// https://mrk21.hatenablog.com/entry/2022/11/10/232922
func NewEntClient(cfg *config.Database) (*ent.Client, error) {
	locale, err := time.LoadLocation("Asia/Tokyo")
	if err != nil {
		return nil, err
	}

	c := mysql.Config{
		User:      cfg.User,
		Passwd:    cfg.Password,
		Net:       "tcp",
		Addr:      fmt.Sprintf("%s:%d", cfg.Host, cfg.Port),
		DBName:    cfg.Name,
		ParseTime: true,
		Loc:       locale,
	}

	drv, err := entsql.Open(cfg.Driver, c.FormatDSN())
	if err != nil {
		return nil, fmt.Errorf("cannot open database: %w", err)
	}

	db := drv.DB()
	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("error pinging to db: %w", err)
	}
	return ent.NewClient(ent.Driver(drv)), nil
}
