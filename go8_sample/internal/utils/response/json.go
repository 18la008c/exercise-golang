package response

import (
	"encoding/json"
	"log"
	"net/http"

	"go8_sample/internal/utils/message"
)

type Standard struct {
	Data interface{} `json:"data"`
	Meta Meta        `json:"meta,omitempty"`
}

type Meta struct {
	Size  int `json:"size"`
	Total int `json:"total"`
}

func Json(w http.ResponseWriter, statusCode int, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	if payload == nil {
		log.Println("Payload is nil. No JSON response will be generated.")
		return
	}

	data, err := json.Marshal(payload)
	if err != nil {
		log.Println(err)
		Error(w, http.StatusInternalServerError, message.ErrInternalError)
		return
	}

	// payloadがnil sliceの場合のみnullになる。 empty sliceはnullではなく `[]` https://tutuz-tech.hatenablog.com/entry/2019/10/20/145302
	if string(data) == "null" {
		_, _ = w.Write([]byte("[]"))
		log.Println("The marshaled data is 'null', responding with an empty array.")
		return
	}

	_, err = w.Write(data)
	if err != nil {
		log.Println(err)
		Error(w, http.StatusInternalServerError, message.ErrInternalError)
		return
	}
}
