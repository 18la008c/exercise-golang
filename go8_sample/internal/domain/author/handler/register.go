package handler

import (
	"github.com/go-chi/chi/v5"

	"go8_sample/internal/domain/author/usecase"
)

func RegisterHTTPEndPoints(router *chi.Mux, u usecase.Author) *Handler {
	h := New(u)

	router.Route("/api/v1/author", func(router chi.Router) {
		router.Get("/{id}", h.Read)
		router.Post("/", h.Create)
	})
	return h
}
