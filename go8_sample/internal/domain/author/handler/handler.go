package handler

import (
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"

	"go8_sample/ent"
	"go8_sample/internal/domain/author/models"
	"go8_sample/internal/domain/author/usecase"
	"go8_sample/internal/utils/binding"
	"go8_sample/internal/utils/message"
	"go8_sample/internal/utils/response"
)

type Handler struct {
	usecase usecase.Author
}

func New(usecase usecase.Author) *Handler {
	return &Handler{usecase: usecase}
}

// Create creates a new author
// @Summary Create an Author
// @Description Create an author using JSON payload
// @Accept json
// @Produce json
// @Param Author body models.CreateRequest true "Create an author using the following format"
// @Success 201 {object} models.Author
// @Failure 400 {string} Bad Request
// @Failure 500 {string} Internal Server Error
// @router /api/v1/author [post]
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	var req models.CreateRequest

	if err := binding.BindJSON(r.Body, &req); err != nil {
		log.Print(err)
		response.Error(w, http.StatusBadRequest, err) // TODO: io.EOF errorを分岐して対応。 TODO: このままだとvalidateのerr内容が直接出るのでwrapするべきか考える
		return
	}

	a, err := h.usecase.Create(r.Context(), &req)
	if err != nil {
		log.Println(err)
		if ent.IsNotFound(err) { // TODO: usecaseでerrをwrappingしてるから、このerrじゃなくなる気がする。。。
			response.Error(w, http.StatusNotFound, message.ErrNoRecord)
			return
		}
		response.Error(w, http.StatusInternalServerError, err)
		return

	}
	response.Json(w, http.StatusCreated, a)
}

// Get a author by its ID
// @Summary Get a Author
// @Description Get a author by its id.
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} models.Author
// @Failure 400 {string} Bad id
// @Failure 500 {string} Internal Server Error
// @router /api/v1/author/{id} [get]
func (h *Handler) Read(w http.ResponseWriter, r *http.Request) {
	authorID, err := models.NewAuthorIDFromString(chi.URLParam(r, "id"))

	if err != nil {
		response.Error(w, http.StatusBadRequest, message.ErrBadRequest)
		return
	}

	a, err := h.usecase.Read(r.Context(), authorID)
	if err != nil {
		if ent.IsNotFound(err) {
			response.Error(w, http.StatusNotFound, message.ErrNoRecord)
			return
		}
		response.Error(w, http.StatusInternalServerError, err)
		return
	}

	response.Json(w, http.StatusOK, a)
}
