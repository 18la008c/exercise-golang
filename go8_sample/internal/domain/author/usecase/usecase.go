package usecase

import (
	"context"

	"go8_sample/internal/domain/author/models"
	"go8_sample/internal/domain/author/repository"
	bookRepository "go8_sample/internal/domain/book/repository"
)

type AuthorUsecase struct {
	authorRepo repository.Author
	bookRepo   bookRepository.Book
}

type Author interface {
	Create(ctx context.Context, a *models.CreateRequest) (*models.Author, error)
	Read(ctx context.Context, id models.AuthorID) (*models.Author, error)
}

func New(authRepo repository.Author, bookRepo bookRepository.Book) *AuthorUsecase {
	return &AuthorUsecase{
		authorRepo: authRepo,
		bookRepo:   bookRepo,
	}
}

func (u *AuthorUsecase) Create(ctx context.Context, req *models.CreateRequest) (*models.Author, error) {

	author, err := models.NewAuthor(req.FirstName, req.MiddleName, req.LastName, req.BookIDs)
	if err != nil {
		return nil, err
	}

	if err := u.authorRepo.Create(ctx, author); err != nil {
		return nil, err
	}
	return author, nil
}

func (u *AuthorUsecase) Read(ctx context.Context, id models.AuthorID) (*models.Author, error) {
	return u.authorRepo.Read(ctx, id)
}
