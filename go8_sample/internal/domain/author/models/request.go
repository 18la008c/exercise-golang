package models

import (
	"go8_sample/internal/domain/book/models"
)

type CreateRequest struct {
	FirstName  string          `json:"first_name" validate:"required"`
	MiddleName string          `json:"middle_name"`
	LastName   string          `json:"last_name" validate:"required"`
	BookIDs    []models.BookID `json:"book_ids"` //TODO: book_idsをoptionにする場合はどうすれば良い？
}
