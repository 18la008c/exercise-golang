package models

import (
	"encoding/json"
	"go8_sample/internal/domain/book/models"
	"go8_sample/internal/utils/message"
	"strings"

	"github.com/google/uuid"
)

type AuthorID uuid.UUID

func (id AuthorID) UUID() uuid.UUID {
	return uuid.UUID(id)
}

func (id AuthorID) String() string {
	return uuid.UUID(id).String()
}

func (id AuthorID) MarshalJSON() ([]byte, error) {
	return json.Marshal(id.String())
}

func (id *AuthorID) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	u, err := uuid.Parse(s)
	if err != nil {
		return err
	}
	*id = AuthorID(u)
	return nil
}

func NewAuthorID() AuthorID {
	return AuthorID(uuid.New())
}

func NewAuthorIDFromString(id string) (AuthorID, error) {
	u, err := uuid.Parse(id)
	if err != nil {
		return AuthorID{}, err
	}
	return AuthorID(u), nil
}

type Author struct {
	ID         AuthorID        `json:"id"`
	FirstName  string          `json:"first_name"`
	MiddleName string          `json:"middle_name"`
	LastName   string          `json:"last_name"`
	BookIDs    []models.BookID `json:"book_ids"`
}

func NewAuthor(firstName string, middleName string, lastName string, bookIDs []models.BookID) (*Author, error) {
	if strings.Contains("error", firstName) {
		return nil, message.ErrBadRequest
	}
	return &Author{
		ID:         NewAuthorID(),
		FirstName:  firstName,
		MiddleName: middleName,
		LastName:   lastName,
		BookIDs:    bookIDs,
	}, nil
}
