package repository

import (
	"context"
	"fmt"

	"go8_sample/ent"
	"go8_sample/internal/domain/author/models"
	bookModels "go8_sample/internal/domain/book/models"
)

type authorRepository struct {
	entClient *ent.Client
}

type Author interface {
	Create(ctx context.Context, a *models.Author) error
	Read(ctx context.Context, id models.AuthorID) (*models.Author, error)
}

func New(entClient *ent.Client) *authorRepository {
	return &authorRepository{
		entClient: entClient,
	}
}

func (r *authorRepository) Create(ctx context.Context, a *models.Author) error {
	// TODO: リレーションするためにはリレーション先のtableをCRUDする必要がある。これはauthorRepo → bookRepo使うことになるが、repo内で別のrepo参照は良くない。。。結局entのcodeが重複するけどいいの？

	books := make([]*ent.Book, len(a.BookIDs)) // make(***, 0)はいける！
	if len(a.BookIDs) != 0 {
		var err error
		for i, bookID := range a.BookIDs {
			books[i], err = r.entClient.Book.Get(ctx, bookID.UUID())
			if err != nil {
				return fmt.Errorf("cannot find bookID%v: %w", bookID, err) // TODO: message.ErrBadRequestとかの方が良い？　 ent.IsNotFound(err)とかもある
			}
		}
	}

	return r.entClient.Author.Create().
		SetFirstName(a.FirstName).
		SetNillableMiddleName(&a.MiddleName). // nillableはnilが取れるtype(ex. pointer)の必要あり。従ってstringは不可なのでpointerにしてあげる
		SetLastName(a.LastName).
		AddBooks(books...). // edgesの場合は値がoptionになるので、 booksが空でもOK
		Exec(ctx)

	// TODO: errを直接返すのではなく、毎回fmt.Errofでwrapしてあげた方が良い？　その方がどこでerrかわかりやすいし
	// if err != nil {
	// 	return nil, fmt.Errorf("models.repository.Create: %w", err)
	// }
}

func (r *authorRepository) Read(ctx context.Context, id models.AuthorID) (*models.Author, error) {
	entAuthor, err := r.entClient.Author.Get(ctx, id.UUID())
	if err != nil {
		return nil, err
	}

	edgeBooks, err := entAuthor.QueryBooks().All(ctx) // TODO: edges先の情報を取得したいなら、DBにqueryが必要
	if err != nil {
		return nil, err
	}

	bookIDs := make([]bookModels.BookID, len(edgeBooks)) // TODO: modelに入れるためにidとしているけど、本当は本の名前の方が良いからdomain objectの考慮が必要。Create→idが良い、Read→nameが良い。
	for i, entBook := range edgeBooks {
		bookIDs[i] = bookModels.BookID(entBook.ID)
	}

	return models.NewAuthor(entAuthor.FirstName, entAuthor.MiddleName, entAuthor.LastName, bookIDs)
}
