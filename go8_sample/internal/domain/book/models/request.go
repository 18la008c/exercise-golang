package models

import "time"

type CreateRequest struct {
	Title         string    `json:"title" validate:"required"`
	PublishedDate time.Time `json:"published_date" validate:"required"` // TODO: 2006-01-02T15:04:05Zだとjsonのstr → timeに変換OK。2006-01-02T15:04:05Z7:00だとNGなので要調査
	ImageURL      string    `json:"image_url" validate:"required"`
	Description   string    `json:"description"`
}

type UpdateRequest struct {
	ID            int       `json:"-"`
	Title         string    `json:"title"`
	PublishedDate time.Time `json:"published_date"`
	ImageURL      string    `json:"image_url"`
	Description   string    `json:"description"`
}
