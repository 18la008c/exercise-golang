package models

import (
	"encoding/json"
	"go8_sample/internal/utils/message"
	"strings"
	"time"

	"github.com/google/uuid"
)

// Domain Objectとして扱う
type Book struct {
	ID            BookID    `json:"id"`
	Title         string    `json:"title"`
	PublishedDate time.Time `json:"published_date"`
	ImageURL      string    `json:"image_url"`
	Description   string    `json:"description"`
}

func NewBook(title string, publishedDate time.Time, imageURL string, description string) (*Book, error) {
	//TODO: ここでちゃんとしたvalidateを入れるべき + message.ErrBadRequestのようなerrorをどこでどの程度細かく定義するか確認すること
	if strings.Contains(title, "error") {
		return nil, message.ErrBadRequest
	}
	return &Book{
		ID:            NewBookID(),
		Title:         title,
		PublishedDate: publishedDate,
		ImageURL:      imageURL,
		Description:   description,
	}, nil
}

type BookID uuid.UUID

func (id BookID) UUID() uuid.UUID {
	return uuid.UUID(id)
}

func (id BookID) String() string {
	return uuid.UUID(id).String()
}

func (id BookID) MarshalJSON() ([]byte, error) {
	return json.Marshal(id.String())
}

func (id *BookID) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	u, err := uuid.Parse(s)
	if err != nil {
		return err
	}
	*id = BookID(u)
	return nil
}

func NewBookID() BookID {
	return BookID(uuid.New())
}

func NewBookIDFromString(s string) (BookID, error) {
	u, err := uuid.Parse(s)
	if err != nil {
		return BookID{}, err
	}
	return BookID(u), nil
}
