package handler

import (
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"

	"go8_sample/ent"
	"go8_sample/internal/domain/book/models"
	"go8_sample/internal/domain/book/usecase"
	"go8_sample/internal/utils/binding"
	"go8_sample/internal/utils/message"
	"go8_sample/internal/utils/response"
)

type Handler struct {
	usecase usecase.Book
}

func New(usecase usecase.Book) *Handler {
	return &Handler{usecase: usecase}
}

// Create creates a new book record
// @Summary Create a Book
// @Description Create a book using JSON payload
// @Accept json
// @Produce json
// @Param Book body models.CreateRequest true "Create a book using the following format"
// @Success 201 {object} models.Book
// @Failure 400 {string} Bad models.CreateRequest
// @Failure 409 {string} Bad models.CreateRequest // TODO: ここのBad <model>ってなに？
// @Failure 500 {string} Internal Server Error
// @router /api/v1/book [post]
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) { //TODO: 重複保存できちゃうので要修正
	var req models.CreateRequest

	if err := binding.BindJSON(r.Body, &req); err != nil {
		log.Println(err)
		response.Error(w, http.StatusBadRequest, err)
		return
	}

	book, err := h.usecase.Create(r.Context(), &req) //TODO: r.Contextって何？
	if err != nil {
		log.Println(err)
		if ent.IsNotFound(err) {
			response.Error(w, http.StatusNotFound, message.ErrNoRecord)
			return
		}
		if ent.IsConstraintError(err) { // TODO: 自作errorで既存のerrを握りつぶすべきじゃない気がする。(wrapすべきなはず！) 　例:Failed creating schema resources: sql/schema: modify "books" table: Error 1062 (23000): Duplicate entry 'a' for key 'books.title'
			response.Error(w, http.StatusConflict, message.ErrAlreadyExist)
			return
		}
		response.Error(w, http.StatusInternalServerError, err)
		return
	}

	response.Json(w, http.StatusCreated, book)
}

// Get a book by its ID
// @Summary Get a Book
// @Description Get a book by its id.
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} models.Book
// @Failure 400 {string} Bad models.CreateRequest
// @Failure 500 {string} Internal Server Error
// @router /api/v1/book/{id} [get]
func (h *Handler) Read(w http.ResponseWriter, r *http.Request) {
	bookID, err := models.NewBookIDFromString(chi.URLParam(r, "id"))
	// bookID, err := strconv.ParseUint(chi.URLParam(r, "id"), 10, 64)
	if err != nil {
		response.Error(w, http.StatusBadRequest, message.ErrBadRequest)
		return
	}

	b, err := h.usecase.Read(r.Context(), bookID)
	if err != nil {
		if ent.IsNotFound(err) {
			response.Error(w, http.StatusNotFound, message.ErrNoRecord)
			return
		}
		response.Error(w, http.StatusInternalServerError, err)
		return
	}

	response.Json(w, http.StatusOK, b)
}

// List will fetch the books based on given params
// @Summary Shows all books
// @Description Lists all books. By default, it gets first page with 30 items.
// @Accept json
// @Produce json
// @Success 200 {object} []models.Book
// @Failure 500 {string} Internal Server Error
// @router /api/v1/book [get]
func (h *Handler) List(w http.ResponseWriter, r *http.Request) {

	books, err := h.usecase.List(r.Context())
	if err != nil {
		log.Println(err)
		response.Error(w, http.StatusInternalServerError, err)
		return
	}

	response.Json(w, http.StatusOK, books)
}
