package handler

import (
	"go8_sample/internal/domain/book/usecase"

	"github.com/go-chi/chi/v5"
)

func RegisterHTTPEndPoints(router *chi.Mux, u usecase.Book) *Handler {
	h := New(u)

	router.Route("/api/v1/book", func(router chi.Router) {
		router.Post("/", h.Create)
		router.Get("/", h.List)
		router.Get("/{id}", h.Read)
	})
	return h
}
