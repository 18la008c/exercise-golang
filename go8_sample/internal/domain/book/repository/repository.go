package repository

import (
	"context"

	"go8_sample/ent"
	"go8_sample/internal/domain/book/models"
)

type Book interface {
	Create(ctx context.Context, book *models.Book) error
	List(ctx context.Context) ([]*models.Book, error)
	Read(ctx context.Context, bookID models.BookID) (*models.Book, error)
	// Update(ctx context.Context, book *book.UpdateRequest) error
	Delete(ctx context.Context, bookID models.BookID) error
	// Search(ctx context.Context, req *book.Filter) ([]*book.Schema, error)
}

type bookRepository struct {
	entClient *ent.Client
}

func New(entClient *ent.Client) *bookRepository {
	return &bookRepository{entClient: entClient}
}

// TODO: 全部詰め替えないといけないの？ Optionalになっている場合どうするの？
func (r *bookRepository) Create(ctx context.Context, book *models.Book) error { //　TODO: 変数一文字にする・しないを明確化する
	return r.entClient.Book.Create().
		SetID(book.ID.UUID()).
		SetTitle(book.Title).
		SetPublishedDate(book.PublishedDate).
		SetImageURL(book.ImageURL).
		SetDescription(book.Description).Exec(ctx)
}

func (r *bookRepository) Read(ctx context.Context, bookID models.BookID) (*models.Book, error) {
	entBook, err := r.entClient.Book.Get(ctx, bookID.UUID())
	if err != nil {
		return nil, err
	}
	return toModel(entBook), nil
}

func (r *bookRepository) List(ctx context.Context) ([]*models.Book, error) {
	entBooks, err := r.entClient.Book.Query().All(ctx)
	if err != nil {
		return nil, err
	}
	books := []*models.Book{}
	for _, entBook := range entBooks {
		books = append(books, toModel(entBook))
	}
	return books, nil
}

func (r *bookRepository) Delete(ctx context.Context, bookID models.BookID) error {
	return r.entClient.Book.DeleteOneID(bookID.UUID()).Exec(ctx)
}

func toModel(record *ent.Book) *models.Book {
	return &models.Book{
		ID:            models.BookID(record.ID),
		Title:         record.Title,
		PublishedDate: record.PublishedDate,
		ImageURL:      record.ImageURL,
		Description:   record.Description,
	}
}
