package repository

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"testing"
	"time"

	"github.com/ory/dockertest"
	"github.com/stretchr/testify/assert"

	"go8_sample/ent"
	"go8_sample/internal/config"
	"go8_sample/internal/domain/book/models"
	"go8_sample/internal/utils/database"
)

var (
	entClient *ent.Client
	cfg       *config.Database = &config.Database{
		Driver:   "mysql",
		Host:     "localhost",
		Port:     3306,
		Name:     "test",
		User:     "root",
		Password: "dockertest", //composeのDBに間違ってアクセスしないように、passwordを分ける。
	}
)

// TODO: packets.go:37: unexpected EOFの解消 → NewEntClientを使うと見えなくなるので注意。
func TestMain(m *testing.M) {
	// Connect to dockerd
	pool, err := dockertest.NewPool("")
	if err != nil {
		log.Panicf("Could not construct pool: %s", err)
	}
	pool.MaxWait = time.Minute * 1
	if err = pool.Client.Ping(); err != nil {
		log.Panicf("Could not connect to Docker: %s", err)
	}

	// Create mysql container
	resource, err := pool.Run("mysql", "8", []string{
		fmt.Sprintf("MYSQL_ROOT_PASSWORD=%s", cfg.Password),
		fmt.Sprintf("MYSQL_TCP_PORT=%d", cfg.Port),
		fmt.Sprintf("MYSQL_DATABASE=%s", cfg.Name),
	})
	if err != nil {
		log.Panicf("Could not start resource: %s", err)
	}
	defer closeContainer(resource, pool)

	// Connect to mysql for test
	port, err := strconv.ParseUint(resource.GetPort("3306/tcp"), 10, 16)
	if err != nil {
		log.Panicln(err)
	}
	cfg := &config.Database{
		Driver:   "mysql",
		Host:     "localhost",
		Port:     uint16(port),
		Name:     "test",
		User:     "root",
		Password: "dockertest", //composeのDBに間違ってアクセスしないように、passwordを分ける。
	}
	if err := pool.Retry(
		func() error {
			// `:=`でentClientを再定義すると、package global変数じゃなくなるので注意
			entClient, err = database.NewEntClient(cfg)
			return err
		},
	); err != nil {
		log.Panicf("Could not connect to database: %s", err)
	}

	ctx := context.Background()
	if err := database.Migrate(ctx, entClient); err != nil {
		log.Panicln(err)
	}

	m.Run()

}

func closeContainer(resource *dockertest.Resource, pool *dockertest.Pool) {
	if err := pool.Purge(resource); err != nil {
		log.Panicf("Could not purge resource: %s", err)
	}
}

func TestRepository_Create(t *testing.T) {
	// createにreadを使ってtest範囲が重複してしまうが、こういうものらしい。
	repo := New(entClient)
	ctx := context.Background()

	expected := newSampleBook(t)

	// OK
	err := repo.Create(ctx, expected)
	assert.Nil(t, err)
	actual, err := repo.Read(ctx, expected.ID)
	assert.Nil(t, err)
	assert.Equal(t, expected, actual) // timezoneがDBとtest側で一致しないと一致扱いにならないので注意! WithinDuration()というassertもある。

	// NG 重複保存error
	err = repo.Create(ctx, expected)
	assert.Equal(t, true, ent.IsConstraintError(err))

	if err := repo.Delete(ctx, expected.ID); err != nil {
		t.Fatal(err)
	}
}

func TestRepository_Read(t *testing.T) {
	repo := New(entClient)
	ctx := context.Background()

	expected := newSampleBook(t)
	// TODO: 本当はtableをclearした方が良い
	if err := repo.Create(ctx, expected); err != nil {
		t.Fatal(err)
	}

	// OK
	actual, err := repo.Read(ctx, expected.ID) // TODO: idを1と決め打ちしてるけど、本来は自分でID生成してmodelに保持する or createでidを返却する必要あり。
	assert.Nil(t, err)
	assert.Equal(t, expected, actual)

	// NG 存在しないrecodeの場合
	unexpectedID := models.NewBookID()
	_, err = repo.Read(ctx, unexpectedID)
	assert.Equal(t, true, ent.IsNotFound(err))
}

func TestRepository_Update(t *testing.T) {
	//Updateの単体テストができない。。。 updateのためにent.Carという別のtableのschemaが必要
	//func (r *userRepository) Update(ctx context.Context, user *ent.User, cars ...*ent.Car)
}

func newSampleBook(t *testing.T) *models.Book {
	parsedTime, err := time.Parse(time.RFC3339, "2023-07-31T12:34:56+09:00")
	if err != nil {
		t.Fatal(err)
	}

	book, err := models.NewBook(
		"TEST BOOK",
		parsedTime,
		"https://example.com",
		"For Test Book",
	)
	if err != nil {
		t.Fatal(err)
	}
	return book
}
