package usecase

import (
	"context"

	"go8_sample/internal/domain/book/models"
	"go8_sample/internal/domain/book/repository"
)

type Book interface {
	Create(ctx context.Context, book *models.CreateRequest) (*models.Book, error)
	List(ctx context.Context) ([]*models.Book, error)
	Read(ctx context.Context, bookID models.BookID) (*models.Book, error)
	// Update(ctx context.Context, book *models.UpdateRequest) (*models.Schema, error)
	// Delete(ctx context.Context, bookID int) error
	// Search(ctx context.Context, req *models.Filter) ([]*models.Schema, error)
}

type BookUsecase struct {
	bookRepo repository.Book //intefaceはpointerにできない。 TODO: じゃあどうするの？
}

func New(bookRepo repository.Book) *BookUsecase {
	return &BookUsecase{
		bookRepo: bookRepo,
	}
}

func (u *BookUsecase) Create(ctx context.Context, req *models.CreateRequest) (*models.Book, error) {
	// TODO: idをdbの自動生成ではなくする場合はここで新規作成が必要
	book, err := models.NewBook(req.Title, req.PublishedDate, req.ImageURL, req.Description)
	if err != nil {
		return nil, err
	}

	if err := u.bookRepo.Create(ctx, book); err != nil {
		return nil, err
	}

	return book, nil
}

func (u *BookUsecase) Read(ctx context.Context, bookID models.BookID) (*models.Book, error) {
	return u.bookRepo.Read(ctx, bookID)
}

func (u *BookUsecase) List(ctx context.Context) ([]*models.Book, error) {
	return u.bookRepo.List(ctx)
}
