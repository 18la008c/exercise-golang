package models

import (
	"encoding/json"
	"errors"
	"regexp"

	"github.com/google/uuid"

	"go8_sample/pkg/argon2id"
)

const (
	minPasswordLength = 8
)

type UserID uuid.UUID

func (id UserID) UUID() uuid.UUID {
	return uuid.UUID(id)
}

func (id UserID) String() string {
	return uuid.UUID(id).String()
}

func (id UserID) MarshalJSON() ([]byte, error) {
	return json.Marshal(id.String())
}

func (id *UserID) UnmarshalJSON(b []byte) error {
	var s string
	if err := json.Unmarshal(b, &s); err != nil {
		return err
	}
	u, err := uuid.Parse(s)
	if err != nil {
		return err
	}
	*id = UserID(u)
	return nil
}

func NewUserID() UserID {
	return UserID(uuid.New())
}

func NewUserIDFromString(id string) (UserID, error) {
	u, err := uuid.Parse(id)
	if err != nil {
		return UserID{}, err
	}
	return UserID(u), nil
}

type User struct {
	ID             UserID `json:"id"`
	Name           string `json:"name"`
	Email          string `json:"email"`
	HashedPassword string `json:"hashed_password"`
}

func validatePassword(password string) error {

	hasSymbol := regexp.MustCompile(`[!@#$%^&*(),.?":{}|<>]`).MatchString(password)
	hasNumber := regexp.MustCompile(`[0-9]`).MatchString(password)
	hasUpperCase := regexp.MustCompile(`[A-Z]`).MatchString(password)

	if (len(password) > minPasswordLength) && hasSymbol && hasNumber && hasUpperCase {
		return nil
	}
	return errors.New("Password must be at least 8 characters long, contain at least one symbol, number and uppercase letter")
}

func NewUser(name string, email string, password string) (*User, error) {
	if err := validatePassword(password); err != nil {
		return nil, err //TODO: このままだと500になるので、errorのwrapが必要
	}
	hashedPassword, err := argon2id.GenerateHash(password, argon2id.RFC_9106_LOW_MEMORY)
	if err != nil {
		return nil, err
	}

	return &User{
		ID:             NewUserID(),
		Name:           name,
		Email:          email,
		HashedPassword: hashedPassword,
	}, nil
}
