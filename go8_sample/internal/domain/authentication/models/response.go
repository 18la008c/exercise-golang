package models

type LoginResponse struct {
	Token string `json:"token"`
}

type RegisterResponse struct {
	ID    UserID `json:"id"`
	Name  string `json:"name"`
	Email string `json:"email"`
}
