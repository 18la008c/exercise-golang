package handler

import (
	"go8_sample/internal/domain/authentication/usecase"
	"go8_sample/internal/middleware"

	"github.com/go-chi/chi/v5"
)

func RegisterHTTPEndPoints(router *chi.Mux, u usecase.Authentication) *Handler {
	h := New(u)

	router.Route("/api/v1/authentication", func(router chi.Router) {
		router.Group(func(router chi.Router) {
			router.Post("/register", h.Register)
			router.Post("/login", h.Login)
		})
		router.Group(func(router chi.Router) {
			router.Use(middleware.Authenticator)
			router.Get("/me", h.Me)
		})
	})
	return h
}
