package handler

import (
	"go8_sample/internal/domain/authentication/models"
	"go8_sample/internal/domain/authentication/usecase"
	"go8_sample/internal/utils/binding"
	"go8_sample/internal/utils/response"
	"log"
	"net/http"
)

type Handler struct {
	usecase usecase.Authentication
}

func New(usecase usecase.Authentication) *Handler {
	return &Handler{usecase: usecase}
}

// Create creates a new user record
// @Summary Create a User
// @Description Create a user using JSON payload
// @Accept json
// @Produce json
// @Param User body models.RegisterRequest true "Create a user using the following format"
// @Success 201 {object} models.LoginResponse
// @Failure 400 {string} Bad models.RegisterRequest
// @Failure 409 {string} Bad models.RegisterRequest
// @Failure 500 {string} Internal Server Error
// @router /api/v1/authentication/register [post]
func (h *Handler) Register(w http.ResponseWriter, r *http.Request) {
	var req models.RegisterRequest

	if err := binding.BindJSON(r.Body, &req); err != nil {
		log.Println(err)
		response.Error(w, http.StatusBadRequest, err)
		return
	}

	user, err := h.usecase.Register(r.Context(), &req)
	if err != nil {
		log.Println(err)
		response.Error(w, http.StatusInternalServerError, err)
		return
	}

	response.Json(w, http.StatusCreated, user)
}

// Login login a user
// @Summary Login a User
// @Description Login a user using JSON payload
// @Accept json
// @Produce json
// @Param User body models.LoginRequest true "Login a user using the following format"
// @Success 200 {object} string
// @Failure 400 {string} Bad models.LoginRequest
// @Failure 500 {string} Internal Server Error
// @router /api/v1/authentication/login [post]
func (h *Handler) Login(w http.ResponseWriter, r *http.Request) {
	var req models.LoginRequest

	if err := binding.BindJSON(r.Body, &req); err != nil {
		log.Println(err)
		response.Error(w, http.StatusBadRequest, err)
		return
	}

	token, err := h.usecase.Login(r.Context(), &req)
	if err != nil {
		log.Println(err)
		response.Error(w, http.StatusInternalServerError, err)
		return
	}

	response.Json(w, http.StatusOK, token)
}

// Me returns the current user
// @Summary Get the current user
// @Description Get the current user
// @Accept json
// @Produce json
// @Security ApiKeyAuth
// @Success 200 {object} string
// @Failure 401 {string} Unauthorized
// @Failure 500 {string} Internal Server Error
// @router /api/v1/authentication/me [get]
func (h *Handler) Me(w http.ResponseWriter, r *http.Request) {
	user, err := h.usecase.Me(r.Context())
	if err != nil {
		log.Println(err)
		response.Error(w, http.StatusInternalServerError, err)
		return
	}

	response.Json(w, http.StatusOK, user)
}
