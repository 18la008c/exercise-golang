package usecase

import (
	"context"
	"errors"

	"go8_sample/internal/domain/authentication/models"
	"go8_sample/internal/domain/authentication/repository"
	"go8_sample/pkg/argon2id"
)

type Authentication interface {
	Register(ctx context.Context, req *models.RegisterRequest) (*models.RegisterResponse, error)
	Login(ctx context.Context, req *models.LoginRequest) (*models.LoginResponse, error)
	Me(ctx context.Context) (*models.RegisterResponse, error)
}

type AuthenticationUsecase struct {
	authenticationRepo repository.Authentication
}

func New(authenticationRepo repository.Authentication) *AuthenticationUsecase {
	return &AuthenticationUsecase{
		authenticationRepo: authenticationRepo,
	}
}

func (u *AuthenticationUsecase) Register(ctx context.Context, req *models.RegisterRequest) (*models.RegisterResponse, error) {
	user, err := models.NewUser(req.Name, req.Email, req.Password)
	if err != nil {
		return nil, err
	}

	if err := u.authenticationRepo.Create(ctx, user); err != nil {
		return nil, err
	}

	return &models.RegisterResponse{
		ID:    user.ID,
		Name:  user.Name,
		Email: user.Email,
	}, nil
}

func (u *AuthenticationUsecase) Login(ctx context.Context, req *models.LoginRequest) (*models.LoginResponse, error) {
	user, err := u.authenticationRepo.ReadByEmail(ctx, req.Email)
	if err != nil {
		return nil, err
	}

	_, err = argon2id.ComparePasswordAndHash(req.Password, user.HashedPassword)
	if err != nil {
		return nil, errors.New("wrong password is provided")
	}

	token, err := GenerateToken(user.ID)
	if err != nil {
		return nil, err
	}

	return &models.LoginResponse{
		Token: token,
	}, nil
}

func (u *AuthenticationUsecase) Me(ctx context.Context) (*models.RegisterResponse, error) {
	userID, err := models.NewUserIDFromString(ctx.Value("userID").(string)) //TODO: 型assertionがうまくいかないと、panicになる。 interface conversion: interface {} is nil, not string
	if err != nil {
		return nil, err
	}
	user, err := u.authenticationRepo.ReadByID(ctx, userID)
	if err != nil {
		return nil, err
	}

	return &models.RegisterResponse{
		ID:    user.ID,
		Name:  user.Name,
		Email: user.Email,
	}, nil
}
