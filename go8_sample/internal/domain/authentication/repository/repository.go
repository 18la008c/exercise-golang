package repository

import (
	"context"
	"go8_sample/ent"
	"go8_sample/ent/user"
	"go8_sample/internal/domain/authentication/models"
)

type Authentication interface {
	Create(ctx context.Context, user *models.User) error
	ReadByEmail(ctx context.Context, email string) (*models.User, error)
	ReadByID(ctx context.Context, id models.UserID) (*models.User, error)
}

type authenticationRepository struct {
	entClient *ent.Client
}

func New(entClient *ent.Client) *authenticationRepository {
	return &authenticationRepository{entClient: entClient}
}

func (r *authenticationRepository) Create(ctx context.Context, user *models.User) error {
	return r.entClient.User.Create().
		SetID(user.ID.UUID()).
		SetName(user.Name).
		SetEmail(user.Email).
		SetHashedPassword(user.HashedPassword).
		Exec(ctx)
}

func (r *authenticationRepository) ReadByEmail(ctx context.Context, email string) (*models.User, error) {
	entUser, err := r.entClient.User.Query().Where(user.Email(email)).Only(ctx)
	if err != nil {
		return nil, err
	}
	return toModel(entUser), nil
}

func (r *authenticationRepository) ReadByID(ctx context.Context, id models.UserID) (*models.User, error) {
	entUser, err := r.entClient.User.Get(ctx, id.UUID())
	if err != nil {
		return nil, err
	}
	return toModel(entUser), nil
}

func toModel(entUser *ent.User) *models.User {
	return &models.User{
		ID:             models.UserID(entUser.ID),
		Name:           entUser.Name,
		Email:          entUser.Email,
		HashedPassword: entUser.HashedPassword,
	}
}
