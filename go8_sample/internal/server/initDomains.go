package server

import (
	"fmt"

	httpSwagger "github.com/swaggo/http-swagger/v2"

	"go8_sample/docs"
	bookHandler "go8_sample/internal/domain/book/handler"
	bookRepo "go8_sample/internal/domain/book/repository"
	BookUsecase "go8_sample/internal/domain/book/usecase"

	authorHandler "go8_sample/internal/domain/author/handler"
	autherRepo "go8_sample/internal/domain/author/repository"
	authorUsecase "go8_sample/internal/domain/author/usecase"

	authHandler "go8_sample/internal/domain/authentication/handler"
	authRepo "go8_sample/internal/domain/authentication/repository"
	authUsecase "go8_sample/internal/domain/authentication/usecase"
)

func (s *Server) InitDomains() {
	s.initSwagger()
	s.initBook()
	s.initAuthor()
	s.initAuthentication()
}

func (s *Server) initSwagger() {
	// https://github.com/swaggo/swag/issues/367
	docs.SwaggerInfo.Host = fmt.Sprintf("%s:%d", s.cfg.API.Host, s.cfg.API.Port)
	s.router.Get("/swagger/*", httpSwagger.Handler())
}

func (s *Server) initBook() {
	r := bookRepo.New(s.entClient)
	u := BookUsecase.New(r)
	bookHandler.RegisterHTTPEndPoints(s.router, u)
}

func (s *Server) initAuthor() {
	br := bookRepo.New(s.entClient) // TODO: initBookとここの両方でrepo作ってるの無駄
	ar := autherRepo.New(s.entClient)

	u := authorUsecase.New(ar, br)
	authorHandler.RegisterHTTPEndPoints(s.router, u)
}

func (s *Server) initAuthentication() {
	r := authRepo.New(s.entClient)
	u := authUsecase.New(r)
	authHandler.RegisterHTTPEndPoints(s.router, u)
}
