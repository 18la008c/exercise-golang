package server

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"go8_sample/ent"
	"go8_sample/internal/config"
	"go8_sample/internal/utils/database"
	"go8_sample/internal/utils/response"
)

type Server struct {
	cfg        *config.Config
	entClient  *ent.Client
	router     *chi.Mux
	httpServer *http.Server
}

func New() (*Server, error) {
	cfg := config.New()

	entClient, err := database.NewEntClient(cfg.Database)
	if err != nil {
		return nil, fmt.Errorf("Failed to connect: %w", err)
	}

	return &Server{
		cfg:       cfg,
		entClient: entClient,
		router:    chi.NewRouter(),
	}, nil

}

func (s *Server) Init() {
	s.router.Use(middleware.Logger)

	s.router.Get("/", func(w http.ResponseWriter, r *http.Request) {
		response.Json(w, http.StatusOK, map[string]string{"message": "welcome"})
	})

	s.InitDomains()
}

func (s *Server) Run() {
	log.Print("run")
	s.httpServer = &http.Server{
		Addr:    fmt.Sprintf("%s:%d", s.cfg.API.Host, s.cfg.API.Port),
		Handler: s.router,
	}

	log.Printf("Serving at %s:%v\n", s.cfg.API.Host, s.cfg.API.Port)
	err := s.httpServer.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}
