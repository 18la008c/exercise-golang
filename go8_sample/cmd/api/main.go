package main

import (
	"go8_sample/internal/server"
	"log"
)

// @title go8_sample
// @version 0.1.0
// @description Go + Postgres + Chi router + sqlx + ent + Testing starter kit for API development.
// @contact.name kato.hiroki
// @contact.url http://example.com
// @contact.email email@example.com
// @host localhost:8888
// @BasePath /
func main() {
	s, err := server.New()
	if err != nil {
		log.Fatalf("Cannot create server instance: %v", err)
	}
	s.Init()
	s.Run()
}
