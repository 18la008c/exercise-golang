package main

import (
	"context"
	"log"

	"go8_sample/internal/config"
	"go8_sample/internal/utils/database"
)

func main() {
	cfg := config.New()
	ctx := context.Background()

	entClient, err := database.NewEntClient(cfg.Database)
	if err != nil {
		log.Fatalf("Failed to connect db by ent: %v", err)
	}
	defer entClient.Close()

	if err := database.Migrate(ctx, entClient); err != nil {
		log.Fatalf("Failed creating schema resources: %v", err)
	}

	log.Println("Database migration successful.")
}
