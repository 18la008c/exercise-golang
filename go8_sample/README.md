# sample
## login
```
curl -i -X POST 192.168.1.203:8888/api/v1/authentication/register \
  -d '{"name": "alpha", "email": "alpha@example.com", "password": "alphaP@ssword1"}' \
  -H 'Content-Type: application/json'

curl -i -X POST 192.168.1.203:8888/api/v1/authentication/login \
  -d '{"email": "alpha@example.com", "password": "alphaP@ssword1"}' \
  -H 'Content-Type: application/json'

curl -i -X GET 192.168.1.203:8888/api/v1/authentication/me \
  -H 'Authorization: Bearer <your_token_here>'
```
## book
```
curl -X 'POST' \
  'http://192.168.1.203:8888/api/v1/book' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "description": "string",
  "image_url": "string",
  "published_date": "2014-10-10T13:50:40+09:00",
  "title": "string"
}'
===
{"id":"6b89b7f5-fa4d-467d-bf52-64babf0f0547","title":"string","published_date":"2014-10-10T13:50:40+09:00","image_url":"string","description":"string"}

curl -X 'GET' \
  'http://192.168.1.203:8888/api/v1/book/6b89b7f5-fa4d-467d-bf52-64babf0f0547' \
  -H 'accept: application/json'
===
{"id":"6b89b7f5-fa4d-467d-bf52-64babf0f0547","title":"string","published_date":"2014-10-10T13:50:40+09:00","image_url":"string","description":"string"}

```