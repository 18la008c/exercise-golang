// Code generated by ent, DO NOT EDIT.

package ent

import (
	"go8_sample/ent/author"
	"go8_sample/ent/book"
	"go8_sample/ent/schema"
	"go8_sample/ent/user"
	"time"

	"github.com/google/uuid"
)

// The init function reads all schema descriptors with runtime code
// (default values, validators, hooks and policies) and stitches it
// to their package variables.
func init() {
	authorFields := schema.Author{}.Fields()
	_ = authorFields
	// authorDescCreatedAt is the schema descriptor for created_at field.
	authorDescCreatedAt := authorFields[4].Descriptor()
	// author.DefaultCreatedAt holds the default value on creation for the created_at field.
	author.DefaultCreatedAt = authorDescCreatedAt.Default.(func() time.Time)
	// authorDescUpdatedAt is the schema descriptor for updated_at field.
	authorDescUpdatedAt := authorFields[5].Descriptor()
	// author.DefaultUpdatedAt holds the default value on creation for the updated_at field.
	author.DefaultUpdatedAt = authorDescUpdatedAt.Default.(func() time.Time)
	// author.UpdateDefaultUpdatedAt holds the default value on update for the updated_at field.
	author.UpdateDefaultUpdatedAt = authorDescUpdatedAt.UpdateDefault.(func() time.Time)
	// authorDescID is the schema descriptor for id field.
	authorDescID := authorFields[0].Descriptor()
	// author.DefaultID holds the default value on creation for the id field.
	author.DefaultID = authorDescID.Default.(func() uuid.UUID)
	bookFields := schema.Book{}.Fields()
	_ = bookFields
	// bookDescCreatedAt is the schema descriptor for created_at field.
	bookDescCreatedAt := bookFields[5].Descriptor()
	// book.DefaultCreatedAt holds the default value on creation for the created_at field.
	book.DefaultCreatedAt = bookDescCreatedAt.Default.(func() time.Time)
	// bookDescUpdatedAt is the schema descriptor for updated_at field.
	bookDescUpdatedAt := bookFields[6].Descriptor()
	// book.DefaultUpdatedAt holds the default value on creation for the updated_at field.
	book.DefaultUpdatedAt = bookDescUpdatedAt.Default.(func() time.Time)
	// book.UpdateDefaultUpdatedAt holds the default value on update for the updated_at field.
	book.UpdateDefaultUpdatedAt = bookDescUpdatedAt.UpdateDefault.(func() time.Time)
	// bookDescID is the schema descriptor for id field.
	bookDescID := bookFields[0].Descriptor()
	// book.DefaultID holds the default value on creation for the id field.
	book.DefaultID = bookDescID.Default.(func() uuid.UUID)
	userFields := schema.User{}.Fields()
	_ = userFields
	// userDescID is the schema descriptor for id field.
	userDescID := userFields[0].Descriptor()
	// user.DefaultID holds the default value on creation for the id field.
	user.DefaultID = userDescID.Default.(func() uuid.UUID)
}
