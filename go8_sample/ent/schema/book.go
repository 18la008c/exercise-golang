package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// Book holds the schema definition for the Book entity.
type Book struct {
	ent.Schema
}

// Fields of the Book.
func (Book) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).Unique().Default(uuid.New),
		field.String("title").Unique(),
		field.Time("published_date"),
		field.String("image_url").Optional(),
		field.String("description").Sensitive(),
		field.Time("created_at").Default(time.Now).StructTag(`json:"-"`), //StructTagはschema.Bookとしたときにstructのtagをつけられる。また "-"はjson化時に無視される。
		field.Time("updated_at").Default(time.Now).UpdateDefault(time.Now).StructTag(`json:"-"`),
	}
}

// Edges of the Book.
func (Book) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("authors", Author.Type),
	}
}
