package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"github.com/google/uuid"
)

// Author holds the schema definition for the Author entity.
type Author struct {
	ent.Schema
}

// Fields of the Author.
func (Author) Fields() []ent.Field {
	return []ent.Field{
		field.UUID("id", uuid.UUID{}).Unique().Default(uuid.New),
		field.String("first_name"),
		field.String("middle_name").Optional(),
		field.String("last_name"),
		field.Time("created_at").Default(time.Now).StructTag(`json:"-"`), //StructTagはschema.Bookとしたときにstructのtagをつけられる。また "-"はjson化時に無視される。
		field.Time("updated_at").Default(time.Now).UpdateDefault(time.Now).StructTag(`json:"-"`),
	}
}

// Edges of the Author.
func (Author) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("books", Book.Type).Ref("authors"),
	}
}
