package main

import (
	"context"
	"database/sql"
	"db_sample3/internal/config"
	"db_sample3/internal/domain/user"
	"fmt"
	"log"

	entsql "entgo.io/ent/dialect/sql"
	"github.com/go-sql-driver/mysql"

	"db_sample3/ent"
)

func NewEntClient(cfg *config.Database) (*ent.Client, error) {
	// create sql object
	c := mysql.Config{
		User:   cfg.User,
		Passwd: cfg.Password,
		Net:    "tcp",
		Addr:   fmt.Sprintf("%s:%d", cfg.Host, cfg.Port),
		DBName: cfg.Name,
	}
	db, err := sql.Open(cfg.Driver, c.FormatDSN())
	if err != nil {
		return nil, fmt.Errorf("cannot connect db: %v", err)
	}

	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("error pinging to db:%v", err)
	}
	log.Printf("Database connection established successfully!")

	// create ent client from sql object
	drv := entsql.OpenDB(cfg.Driver, db)
	return ent.NewClient(ent.Driver(drv)), nil
}

func main() {
	cfg := config.New()
	log.Printf("%+v", cfg) //&{Database:0xc0000a4180}とsturctの中身もpointerの時どうやってprintするか分からん

	ctx := context.Background()
	entClient, err := NewEntClient(cfg.Database)
	if err != nil {
		log.Fatalf("Faild to connect db: %v", err)
	}

	// auto migrate database by /ent/schema
	if err := entClient.Schema.Create(ctx); err != nil {
		log.Fatalf("Failed creating schema resources: %v", err)
	}

	userRepo := user.NewRepository(entClient)

	//1生成ごとにerrチェックは超面倒では？？？
	u1, err := userRepo.Create(ctx, "alpha", 10)
	if err != nil {
		log.Fatalf("Failed creating user: %v", err)
	}
	u2, err := userRepo.Create(ctx, "bravo", 11)
	if err != nil {
		log.Fatalf("Failed creating user: %v", err)
	}
	u3, err := userRepo.Create(ctx, "charlie", 12)
	if err != nil {
		log.Fatalf("Failed creating user: %v", err)
	}
	fmt.Print(u1, u2, u3)
}
