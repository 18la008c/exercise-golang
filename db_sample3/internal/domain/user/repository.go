package user

import (
	"context"
	"db_sample3/ent"
	"db_sample3/ent/user"
	"fmt"
	"log"
)

type userRepository struct { //　TODO: なんでrepositoryってprivateなんだっけ？
	entClient *ent.Client
}

func NewRepository(entClient *ent.Client) *userRepository {
	return &userRepository{entClient: entClient}
}

func (r *userRepository) Create(ctx context.Context, name string, age uint8) (*ent.User, error) {
	u, err := r.entClient.User.Create().SetAge(age).SetName(name).Save(ctx)
	if err != nil {
		return nil, fmt.Errorf("faild creating user: %v", err)
	}
	log.Printf("user was created: %v", u)

	return u, nil
}

func (r *userRepository) Read(ctx context.Context, name string) (*ent.User, error) {
	u, err := r.entClient.User.Query().Where(user.Name(name)).Only(ctx)
	if err != nil {
		return nil, fmt.Errorf("faild read a user: %v", err)
	}
	return u, nil
}

func (r *userRepository) List(ctx context.Context, name string) ([]*ent.User, error) {
	u, err := r.entClient.User.Query().All(ctx)
	if err != nil {
		return nil, fmt.Errorf("faild read a user: %v", err)
	}
	return u, nil
}

func (r *userRepository) Update(ctx context.Context, user *ent.User, cars ...*ent.Car) (*ent.User, error) {
	//TODO: これだとupdateするfieldごとに、repositoryのmethodが必要で面倒では？ gorm, sqlalchemyはupdate用のclassでoverrideする感じだったから良かったけど。。。
	u, err := user.Update().AddCars(cars...).Save(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed update user: %v", err)
	}
	return u, nil
}
