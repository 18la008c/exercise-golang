package user

//TODO: go test internal/domain/repository_test.goまで指定しないとtestが実行できない。 go testだけでも実行できるようにしたい
import (
	"context"
	"db_sample3/ent"
	"fmt"
	"log"

	"entgo.io/ent/dialect"
	_ "github.com/mattn/go-sqlite3"
)

func ConnectTestDB() (context.Context, *ent.Client) { //TODO: わざわざtestdb接続用の関数用意しているけど、configを使ってmysqlの場合と1つにまとめたいよ

	client, err := ent.Open(dialect.SQLite, "file:ent?mode=memory&cache=shared&_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}

	ctx := context.Background()

	if err := client.Schema.Create(ctx); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	return ctx, client
}

func Example_create() {
	ctx, client := ConnectTestDB()
	userRepo := NewRepository(client)

	u, err := userRepo.Create(ctx, "alpha", 10)
	if err != nil {
		log.Fatalf("Failed creating user: %v", err)
	}
	fmt.Print(u)
	//Output:
	// User(id=1, age=10, name=alpha)
}

func Example_read() { //TODO: userの作成はしてないので、単体でtestは完結せず、前後のtestに左右されるよ？ userの作成もするとcreateのtestも二重ですることになるよ？
	ctx, client := ConnectTestDB() //TODO: 毎回定義しないといけないと何とかならないか？
	userRepo := NewRepository(client)

	u, err := userRepo.Read(ctx, "alpha")
	if err != nil {
		log.Fatalf("Failed creating user: %v", err)
	}
	fmt.Print(u)
	//Output:
	// User(id=1, age=10, name=alpha)
}
