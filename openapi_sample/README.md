# How to use

## 1. execute
```
python -m http.server
docker run --rm -v "${PWD}:/local" openapitools/openapi-generator-cli generate \
    -i http://192.168.1.203:8000/swagger.json \
    -g go \
    -o /local/out/go \
    --global-property skipFormModel=false \
	-p enumClassPrefix=true
    --openapi-normalizer REFACTOR_ALLOF_WITH_PROPERTIES_ONLY=true
```