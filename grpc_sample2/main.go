package main

import "fmt"

type Sample struct {
	name string
	age  int8
}

func main() {
	s := Sample{
		name: "hiroki",
		age:  10,
	}
	fmt.Println(s)
}
