minikube delete -all
minikube start --force --driver=docker
eval $(minikube docker-env)

docker build -t local/gcd -f Dockerfile.gcd .
docker build -t local/api -f Dockerfile.api .

kubectl apply -f api.yaml
kubectl apply -f gcd.yaml