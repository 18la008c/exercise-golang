package main

import (
	"context"
	gcdpb "gcd/pkg/grpc"
	"log"
	"net"
	"os"
	"os/signal"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type myServer struct {
	gcdpb.UnimplementedGCDServiceServer
}

func (s *myServer) Compute(ctx context.Context, r *gcdpb.GCDRequest) (*gcdpb.GCDResponse, error) {
	a, b := r.A, r.B
	for b != 0 {
		a, b = b, a%b
	}
	return &gcdpb.GCDResponse{Result: a}, nil

}

func NewMyServer() *myServer {
	return &myServer{}
}

func main() {
	listener, err := net.Listen("tcp", ":3000")
	if err != nil {
		log.Fatalf("Failed to listen :%v", err)
	}

	s := grpc.NewServer()

	gcdpb.RegisterGCDServiceServer(s, NewMyServer())

	reflection.Register(s)

	go func() {
		log.Printf("start gRPC server port: %v", ":3000")
		s.Serve(listener)
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("stopping gRPC server...")
	s.GracefulStop()

}
