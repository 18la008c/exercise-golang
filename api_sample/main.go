// https://blog.logrocket.com/rest-api-golang-gin-gorm/
package main

import (
	"net/http"

	"github.com/gin-gonic/gin"

	"api_sample/controllers" // new
	"api_sample/models"
)

func main() {
	r := gin.Default()

	models.ConnectDatabase()

	//curl localhost:8080
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "hello world"})
	})

	//curl localhost:8080/books
	r.GET("/books", controllers.FindBooks)

	//curl -d '{"title": "Start with Why", "author": "Simon Sinek"}' -X POST localhost:8080/books
	r.POST("/books", controllers.CreateBook)

	//curl localhost:8080/books/1
	r.GET("/books/:id", controllers.FindBook)

	//curl -d '{"title": "The Infinite Game"}' -X PATCH localhost:8080/books/1
	r.PATCH("/books/:id", controllers.UpdateBook)

	//curl -X DELETE localhost:8080/books/1
	r.DELETE("books/:id", controllers.DeleteBook)

	err := r.Run()
	if err != nil {
		return
	}
}
