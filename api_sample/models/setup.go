// models/setup.go

package models

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

// goでは関数外で定義した場合は、pkg内でglobal変数となる。
// https://dev-yakuza.posstree.com/golang/variables/

var DB *gorm.DB

func ConnectDatabase() {

	database, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	err = database.AutoMigrate(&Book{})
	if err != nil {
		return
	}

	// DBはpkgグローバル変数なので、returnをしなくてもここで定義するだけ良い。
	// code的に何を参照しているか分かりにくくなるので、推奨パターンではない気がする。
	DB = database
}
