// models/book.go

package models

// jsonとgolangで命名規則が異なるから、json:idのような記述が必要らしい。
type Book struct {
	ID     uint   `json:"id" gorm:"primary_key"`
	Title  string `json:"title"`
	Author string `json:"author"`
}
