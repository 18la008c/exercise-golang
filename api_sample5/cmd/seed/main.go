package main

import (
	"api_sample5/config"
	"api_sample5/database"
	db "api_sample5/third_party/database"
	"log"
)

func main() {
	cfg := config.New()
	store := db.NewSqlx(cfg.Database)

	seeder := database.Seeder(store.DB)
	seeder.SeedUsers()
	log.Println("seeding completed.")
}
