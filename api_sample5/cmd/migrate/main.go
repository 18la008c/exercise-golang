package main

import (
	"crypto/rand"
	"fmt"
	"math/big"
)

func main() {
	max := big.NewInt(100)
	randomInt, err := rand.Int(rand.Reader, max)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	fmt.Println("Random Number (0-99):", randomInt)
}
