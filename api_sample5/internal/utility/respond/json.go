package respond

import (
	"encoding/json"
	"log"
	"net/http"

	"api_sample5/internal/utility/message"
)

type Standard struct {
	Data interface{} `json:"data"`
	Meta Meta        `json:"meta,omitempty"` //omitemtpyはstructが空の時jsonでは省略される
}

type Meta struct {
	Size  int `json:"size"`
	Total int `json:"total"`
}

func Json(w http.ResponseWriter, statusCode int, payload interface{}) {
	//status codeのsetも、headerのsetもnet/httpだと自分でやる。
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	if payload == nil {
		return
	}

	data, err := json.Marshal(payload) //Marshalは struct2json
	if err != nil {
		log.Println(err)
		// http.StatusInternalServerErrorはただのintで500のこと
		Error(w, http.StatusInternalServerError, message.ErrInternalError) //errorも自分で定義する必要あり
		return
	}

	if string(data) == "null" {
		_, _ = w.Write([]byte("[]"))
		return
	}

	_, err = w.Write(data)
	if err != nil {
		log.Println(err)
		Error(w, http.StatusInternalServerError, message.ErrInternalError)
		return
	}
}
