//なぜこんなにdb関連のfileを分けるのか。もっとまとめておくべきでは？
// third_party/database, internal/utility/database

package database

import (
	"database/sql"
	"log"
	"math/rand"
	"time"
)

func Alive(db *sql.DB) {
	base, capacity := time.Second, time.Minute

	for backoff := base; backoff < capacity; backoff <<= 1 {
		_, err := db.Exec("SELECT true")
		if err == nil {
			log.Println("database connected")
			return
		}

		if backoff > capacity {
			backoff = capacity
		}
		jitter := rand.Int63n(int64(backoff * 3 / 10))
		sleep := backoff + time.Duration(jitter)

		log.Printf("Database connection failed. Retry after %s s\n", sleep)
		time.Sleep(sleep)
	}

	log.Fatal("Max backoff time exceeded. Exiting...")
}
