package usecase

import (
	"context"

	"api_sample5/internal/domain/book"
	"api_sample5/internal/domain/book/repository"
)

type Book interface {
	Create(ctx context.Context, book *book.CreateRequest) (*book.Schema, error)
}

type BookUseCase struct {
	bookRepo repository.Book
}

func New(bookRepo repository.Book) *BookUseCase {
	return &BookUseCase{
		bookRepo: bookRepo,
	}
}

func (u *BookUseCase) Create(ctx context.Context, book *book.CreateRequest) (*book.Schema, error) {
	bookID, err := u.bookRepo.Create(ctx, book)
	if err != nil {
		return nil, err
	}

	bookFound, err := u.bookRepo.Read(ctx, bookID)
	if err != nil {
		return nil, err
	}
	return bookFound, err
}
