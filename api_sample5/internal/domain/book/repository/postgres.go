package repository

import (
	"context"
	"database/sql"
	"errors"

	"github.com/jmoiron/sqlx"

	"api_sample5/internal/domain/book"
	"api_sample5/internal/utility/message"
)

type Book interface {
	Create(ctx context.Context, book *book.CreateRequest) (int, error)
	Read(ctx context.Context, bookID int) (*book.Schema, error)
}

type bookRepository struct {
	db *sqlx.DB
}

const (
	InsertIntoBooks         = "INSERT INTO books (title, published_date, image_url, description) VALUES ($1, $2, $3, $4) RETURNING id"
	SelectFromBooks         = "SELECT * FROM books ORDER BY created_at DESC"
	SelectFromBooksPaginate = "SELECT * FROM books ORDER BY created_at DESC LIMIT $1 OFFSET $2"
	SelectBookByID          = "SELECT * FROM books where id = $1"
	UpdateBook              = "UPDATE books set title = $1, description = $2, published_date = $3, image_url = $4 where id = $5 RETURNING id"
	DeleteByID              = "DELETE FROM books where id = ($1) RETURNING id"
	SearchBooks             = "SELECT * FROM books where title like '%' || $1 || '%' and description like '%'|| $2 || '%' ORDER BY published_date DESC"
	SearchBooksPaginate     = "SELECT * FROM books where title like '%' || '%' || $1 || '%' || '%' and description like '%'|| $2 || '%' ORDER BY published_date DESC LIMIT $3 OFFSET $4"
)

// memo factory関数によって、structの初期化漏れがなくなる。関数だと引数漏れはerrorになるから
func New(db *sqlx.DB) *bookRepository {
	return &bookRepository{db: db}
}

func (r *bookRepository) Create(ctx context.Context, req *book.CreateRequest) (bookID int, err error) {
	if err = r.db.QueryRowContext(
		ctx, InsertIntoBooks, req.Title, req.PublishedDate, req.ImageURL, req.Description).Scan(&bookID); err != nil {
		return 0, errors.New("repository.Book.Create")

	}
	return bookID, nil
}

func (r *bookRepository) Read(ctx context.Context, bookID int) (*book.Schema, error) {
	var b book.Schema
	err := r.db.GetContext(ctx, &b, SelectBookByID, bookID)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, message.ErrBadRequest
		}
	}

	return &b, err
}
