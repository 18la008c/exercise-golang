package handler

import (
	"api_sample5/internal/domain/book/usecase"

	"github.com/go-chi/chi"
)

func RegisterHTTPEndPoints(router *chi.Mux, uc usecase.Book) *Handler {
	h := NewHandler(uc)

	router.Route("/api/v1/book", func(router ch))

}
