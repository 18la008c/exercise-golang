package handler

import (
	"api_sample5/internal/domain/book"
	"api_sample5/internal/domain/book/usecase"
	"api_sample5/internal/utility/message"
	"api_sample5/internal/utility/respond"
	"database/sql"
	"encoding/json"
	"errors"
	"net/http"
)

type Handler struct {
	useCase usecase.Book
}

func NewHandler(useCase usecase.Book) *Handler {
	return &Handler{
		useCase: useCase,
	}
}

// Create creates a new book record
// @Summary Create a Book
// @Description Create a book using JSON payload
// @Accept json
// @Produce json
// @Param Book body book.CreateRequest true "Create a book using the following format"
// @Success 201 {object} book.Res
// @Failure 400 {string} Bad book.CreateRequest
// @Failure 500 {string} Internal Server Error
// @router /api/v1/book [post]
func (h *Handler) Create(w http.ResponseWriter, r *http.Request) {
	var bookRequest book.CreateRequest
	// memo unmarshalとの違い https://qiita.com/Coolucky/items/44f2bc6e32ca8e9baa96
	err := json.NewDecoder(r.Body).Decode(&bookRequest)
	if err != nil {
		respond.Error(w, http.StatusBadRequest, nil)
		return
	}

	// errs := validate.Validate(h.validate, bookRequest)
	// if errs != nil {
	// 	respond.Errors(w, http.StatusBadRequest, errs)
	// 	return
	// }

	bk, err := h.useCase.Create(r.Context(), &bookRequest)
	if err != nil {
		// memo go8だと、err == sql.ErrNoRowsになってる。。。この差は？
		if errors.Is(err, sql.ErrNoRows) {
			respond.Error(w, http.StatusBadRequest, message.ErrBadRequest)
			return
		}
		respond.Error(w, http.StatusInternalServerError, err)
		return
	}

	b := book.Resource(bk)

	respond.Json(w, http.StatusCreated, b)
}
