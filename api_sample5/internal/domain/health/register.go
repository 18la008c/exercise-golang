package health

import "github.com/go-chi/chi"

func RegisterHTTPEndPoints(router *chi.Mux, uc UseCase) *Handler {
	h := NewHandler(uc)

	router.Route(
		"/api/health",
		func(router chi.Router) {

			router.Get("/", h.Health)
			router.Get("/readiness", h.Readiness)
		},
	)

	return h
}
