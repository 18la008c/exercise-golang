package server

import (
	"context"
	"database/sql"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-chi/chi"
	"github.com/jmoiron/sqlx"
	"github.com/redis/go-redis/v9"

	"api_sample5/config"
	db "api_sample5/third_party/database"
)

const (
	swaggerDocsAssetPath = "./docs/"
)

type Server struct {
	Version string
	cfg     *config.Config

	db   *sql.DB
	sqlx *sqlx.DB //memo なんでsqlとsqlx両方保持してるの？
	//ent  *gen.Client //memo なんでsqlxもentも使ってるの？

	cache   *redis.Client
	cluster *redis.ClusterClient

	//session *scs.SessionManager
	//sessionCloser *postgresstore.PostgresStore

	//validator *validator.Validate
	//cors   *cors.Cors
	router *chi.Mux

	httpServer *http.Server
}

type Options func(opts *Server) error //memo このOptionsって具体的に何？

func New(opts ...Options) *Server {
	s := defaultServer()

	for _, opt := range opts {
		err := opt(s)
		if err != nil {
			log.Fatalln(err)
		}
	}
	return s
}

func defaultServer() *Server {
	return &Server{
		cfg:    config.New(), //memo 毎回newで生成するより、使いまわせないの？
		router: chi.NewRouter(),
	}
}

func (s *Server) Init() {
	// s.setCors()
	// s.newRedis()
	s.NewDatabase()
	// s.newValidator()
	// s.newAuthentication()
	s.newRouter()
	// s.setGlobalMiddleware()
	s.InitDomains()
}

func (s *Server) NewDatabase() {
	if s.cfg.Database.Driver == "" {
		log.Fatal("please fill in database credentials in .env file or set in environment variable")
	}

	s.sqlx = db.NewSqlx(s.cfg.Database)
	//以下のoptionは、third_party/Databaseにまとめるべきでは？？　なぜここでわざわざ設定するのか？
	s.sqlx.SetMaxOpenConns(s.cfg.Database.MaxConnectionPool)
	s.sqlx.SetMaxIdleConns(s.cfg.Database.MaxIdleConnections)
	s.sqlx.SetConnMaxLifetime(s.cfg.Database.ConnectionsMaxLifeTime)
	s.db = s.sqlx.DB
}

// memo defaultServer()で作ってるのになんでこっちでも作ってるのか？
func (s *Server) newRouter() {
	s.router = chi.NewRouter()
}

// memo なぜgetterが用意されているのか？　pointerで渡してるからgetterとしても期待できない
func (s *Server) Config() *config.Config {
	return s.cfg
}

func (s *Server) Run() {
	s.httpServer = &http.Server{
		Addr:              s.cfg.Api.Host + ":" + s.cfg.Api.Port,
		Handler:           s.router,
		ReadHeaderTimeout: s.cfg.Api.ReadHeaderTimeout,
	}
	// memo: なぜgo-routineを使っているのか？
	go func() {
		start(s)
	}()

	//memo: gracefulShutdownをつけないと、すぐgo-routineが終了しちゃう
	gracefulShutdown(context.Background(), s)

}

func start(s *Server) {
	log.Printf("Serving at %s:%s\n", s.cfg.Api.Host, s.cfg.Api.Port)
	log.Fatal(s.httpServer.ListenAndServe())
}

func WithVersion(version string) Options {
	return func(opts *Server) error {
		log.Printf("Starting API version: %s\n", version)
		opts.Version = version
		return nil
	}
}

func gracefulShutdown(ctx context.Context, s *Server) error {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	<-quit

	log.Println("Shutting down...")

	ctx, shutdown := context.WithTimeout(ctx, s.cfg.Api.GracefulTimeout*time.Second)
	defer shutdown()

	err := s.httpServer.Shutdown(ctx)
	if err != nil {
		log.Println(err)
	}

	return nil
}
