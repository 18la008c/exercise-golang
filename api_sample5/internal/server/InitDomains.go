package server

import (
	"net/http"

	"github.com/go-chi/chi"

	_ "api_sample5/docs"
	bookRepo "api_sample5/internal/domain/book/repository"
	bookUseCase "api_sample5/internal/domain/book/usecase"
	"api_sample5/internal/domain/health"
	"api_sample5/internal/utility/respond"

	httpSwagger "github.com/swaggo/http-swagger/v2"
)

func (s *Server) InitDomains() {
	s.initVersion()
	s.initHealth()
	s.initSwagger()

}

func (s *Server) initVersion() {
	s.router.Route(
		"/version",
		func(router chi.Router) {
			//本来以下でjsonのheaderを付与してるけど、responde.Jsonで同様の処理をしているのでいらないのでは？
			//router.Use(middleware.Json)

			router.Get(
				"/",
				func(w http.ResponseWriter, r *http.Request) {
					respond.Json(w, http.StatusOK, map[string]string{"version": s.Version})
				},
			)
		},
	)
}

func (s *Server) initHealth() {
	newHealthRepo := health.NewRepo(s.sqlx)
	newHealthUseCase := health.New(newHealthRepo)
	health.RegisterHTTPEndPoints(s.router, newHealthUseCase)
}

// memo よく分からんけど以下でだけでOK https://github.com/swaggo/http-swagger
func (s *Server) initSwagger() {
	s.router.Get("/swagger/*", httpSwagger.Handler())

	s.router.Get("/swagger", func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, "/swagger/", http.StatusMovedPermanently)
	})
}

func (s *Server) initBook() {
	newBookRepo := bookRepo.New(s.sqlx)
	newBookUseCase := bookUseCase.New(newBookRepo)
	bookHandler.RegisterHTTPEndPoints(s.router, newBookUseCase)
}
