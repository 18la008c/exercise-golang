package main

import "fmt"

type Sample struct {
	v int
}

func (s *Sample) PrintByPointer() {
	fmt.Println(&s.v)
}

func (s Sample) PrintByValue() {
	fmt.Println(&s.v)
}

func main() {
	s := Sample{100}
	fmt.Println(&s.v)
	s.PrintByPointer()
	s.PrintByValue()
}
