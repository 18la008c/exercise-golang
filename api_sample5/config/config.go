package config

import (
	"log"

	"github.com/joho/godotenv"
)

type Config struct {
	Api
	Cache
	Cors
	Database
}

// *はpointerを示す。 &は変数 → pointer
func New() *Config {
	err := godotenv.Load()
	if err != nil {
		log.Println(err)
	}

	return &Config{
		Api:      API(),
		Cache:    NewCache(),
		Cors:     NewCors(),
		Database: DataStore(),
	}
}
