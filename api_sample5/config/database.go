package config

import (
	"time"

	"github.com/kelseyhightower/envconfig"
)

// このtagはenvconfig(pythonのpydantic.BaseConfigと一緒)で使う
// https://qiita.com/andromeda/items/c5195307cd08537d4fad
type Database struct {
	Driver                 string        `required:"true"`
	Host                   string        `default:"localhost"`
	Port                   uint16        `default:"5432"`
	Name                   string        `default:"postgres"`
	TestName               string        `split_words:"true" default:"test"`
	User                   string        `default:"postgres"`
	Pass                   string        `default:"password"`
	SslMode                string        `default:"password"`
	MaxConnectionPool      int           `split_words:"true" default:"disable"`
	MaxIdleConnections     int           `split_words:"true" default:"4"`
	ConnectionsMaxLifeTime time.Duration `split_words:"true" default:"300s"`
}

func DataStore() Database {
	var db Database
	envconfig.MustProcess("DB", &db) //MustProcessはerrでpanicを起こす。 err != nilが要らない。 第一引数にDBと入れると、勝手にDB_***でenvを探す。
	return db
}
