package tasks

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"time"

	"github.com/hibiken/asynq"
)

const (
	TypeEmailDelivery = "email:deliver"
	TypeImageResize   = "image:resize"
)

type EmailDeliveryPayload struct {
	UserID     int
	TemplateID string
}

type ImageResizePayload struct {
	SourceURL string
}

// 非同期タスクを作成する
func NewEmailDeliveryTask(userID int, templateID string) (*asynq.Task, error) {
	payload, err := json.Marshal(EmailDeliveryPayload{UserID: userID, TemplateID: templateID})
	if err != nil {
		return nil, err
	}
	return asynq.NewTask(TypeEmailDelivery, payload), nil
}

func NewImageResizeTask(sourceURL string) (*asynq.Task, error) {
	payload, err := json.Marshal(ImageResizePayload{SourceURL: sourceURL})
	if err != nil {
		return nil, err
	}
	return asynq.NewTask(TypeImageResize, payload, asynq.MaxRetry(5), asynq.Timeout(10*time.Minute)), nil
}

// 非同期タスクを処理する
// 名称は、HandlerxxxTaskが推奨
// asynq.HanderFunc interfaceを満たす必要がある。 func(ctx *context.Context, t *asynq.Task) errorが必要
func HandleEmailDeliveryTask(ctx context.Context, t *asynq.Task) error {
	var p EmailDeliveryPayload
	if err := json.Unmarshal(t.Payload(), &p); err != nil {
		return fmt.Errorf("json.Unmarshal failed: %v: %w", err, asynq.SkipRetry) //asynqは失敗すると自動でretryするが、SkipRetryを返すとretryしない
	}
	log.Printf("delivering email to user %d with template %s", p.UserID, p.TemplateID)
	// 実際にメール送信する処理はmockする。
	log.Printf("mock: sending email to user %d", p.UserID)
	return nil
}

// 非同期タスクを処理するpart2
// 関数じゃなくて、asynq.Hander interfaceを満たす構造体を作成することもできる
// ProcessTask(*asynq.Context, *asynq.Task) errorのmethodの実装が必須
type ImageProcesser struct {
}

func (processor *ImageProcesser) ProcessTask(ctx context.Context, t *asynq.Task) error {
	var p ImageResizePayload
	if err := json.Unmarshal(t.Payload(), &p); err != nil {
		return fmt.Errorf("json.Unmarshal failed: %v: %w", err, asynq.SkipRetry)
	}
	log.Printf("resizing image from %s", p.SourceURL)
	// 実際に画像リサイズする処理はmockする。
	log.Printf("mock: resizing image from %s", p.SourceURL)
	return nil
}

func NewImageProcessor() *ImageProcesser {
	return &ImageProcesser{}
}
