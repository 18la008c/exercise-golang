package main

import (
	"context"
	"log"
	"time"

	"github.com/hibiken/asynq"
	"gitlab.com/18la008c/exercise-golang/asynq_sample/tasks"
)

const redisAddr = "localhost:6379"

func loggingMiddleware(h asynq.Handler) asynq.Handler {
	return asynq.HandlerFunc(func(ctx context.Context, t *asynq.Task) error {
		start := time.Now()
		log.Printf("[logging] Start processing %q", t.Type())
		err := h.ProcessTask(ctx, t)
		if err != nil {
			return err
		}
		log.Printf("[logging] Finished processing %q: Elapsed Time = %v", t.Type(), time.Since(start))
		return nil
	})
}

func sampleMiddleware(h asynq.Handler) asynq.Handler {
	return asynq.HandlerFunc(func(ctx context.Context, t *asynq.Task) error {
		log.Printf("[sample] Before processing %q", t.Type())
		err := h.ProcessTask(ctx, t)
		if err != nil {
			return err
		}
		log.Printf("[sample] After processing %q", t.Type())
		return nil
	})
}

func main() {
	server := asynq.NewServer(
		asynq.RedisClientOpt{Addr: redisAddr},
		asynq.Config{
			Concurrency: 10,
			Queues: map[string]int{
				"critical": 6,
				"default":  3,
				"low":      1,
			},
		},
	)

	mux := asynq.NewServeMux()
	// middlwareはUseを使った順番に実行される
	// この場合はloggingMiddleware -> sampleMiddlewareの順番で実行される
	mux.Use(
		loggingMiddleware,
		sampleMiddleware,
	)

	mux.HandleFunc(tasks.TypeEmailDelivery, tasks.HandleEmailDeliveryTask)
	mux.Handle(tasks.TypeImageResize, tasks.NewImageProcessor())

	if err := server.Run(mux); err != nil {
		log.Fatalf("could not run server: %v", err)
	}
}

// root@v0-dev-01:~/project/exercise-golang/asynq_sample2# go run cmd/worker/main.go
// asynq: pid=1746856 2024/05/20 06:39:05.301833 INFO: Starting processing
// asynq: pid=1746856 2024/05/20 06:39:05.301857 INFO: Send signal TSTP to stop processing new tasks
// asynq: pid=1746856 2024/05/20 06:39:05.301860 INFO: Send signal TERM or INT to terminate the process
// 2024/05/20 06:39:27 [logging] Start processing "email:deliver"
// 2024/05/20 06:39:27 [sample] Before processing "email:deliver"
// 2024/05/20 06:39:27 delivering email to user 1111 with template some:template:id
// 2024/05/20 06:39:27 mock: sending email to user 1111
// 2024/05/20 06:39:27 [sample] After processing "email:deliver"
// 2024/05/20 06:39:27 [logging] Finished processing "email:deliver": Elapsed Time = 140.212µs

// 2024/05/20 06:39:27 [logging] Start processing "image:resize"
// 2024/05/20 06:39:27 [sample] Before processing "image:resize"
// 2024/05/20 06:39:27 resizing image from https://example.com/myassets/image.jpg
// 2024/05/20 06:39:27 mock: resizing image from https://example.com/myassets/image.jpg
// 2024/05/20 06:39:27 [sample] After processing "image:resize"
// 2024/05/20 06:39:27 [logging] Finished processing "image:resize": Elapsed Time = 69.185µs
