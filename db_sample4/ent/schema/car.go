package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/field"
)

// Car holds the schema definition for the Car entity.
type Car struct {
	ent.Schema
}

// Fields of the Car.
func (Car) Fields() []ent.Field {
	return []ent.Field{
		field.String("model"),
		field.Time("registered_at").Default(time.Now),
		field.Bool("registered").Default(false),
	}
}

// Edges of the Car.
func (Car) Edges() []ent.Edge {
	return nil
}
