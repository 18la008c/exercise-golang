-- Disable the enforcement of foreign-keys constraints
PRAGMA foreign_keys = off;
-- Create "new_cars" table
CREATE TABLE `new_cars` (`id` integer NOT NULL PRIMARY KEY AUTOINCREMENT, `model` text NOT NULL, `registered_at` datetime NOT NULL, `registered` bool NOT NULL DEFAULT (false));
-- Copy rows from old table "cars" to new temporary table "new_cars"
INSERT INTO `new_cars` (`id`, `model`, `registered_at`) SELECT `id`, `model`, `registered_at` FROM `cars`;
-- Drop "cars" table after copying rows
DROP TABLE `cars`;
-- Rename temporary table "new_cars" to "cars"
ALTER TABLE `new_cars` RENAME TO `cars`;
-- Enable back the enforcement of foreign-keys constraints
PRAGMA foreign_keys = on;
