package main

import (
	"context"
	"db_sample4/ent"
	"fmt"
	"log"

	"entgo.io/ent/dialect"
)

func main() {

	// client, err := ent.Open(dialect.SQLite, "file:ent?mode=memory&cache=shared&_fk=1")
	client, err := ent.Open(dialect.SQLite, "file:test.db?mode=rw&cache=shared&_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
	defer client.Close()

	ctx := context.Background()

	u1, err := CreateUser(ctx, client, "alpha", 10)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Create user test: %v\n", u1)
}

func CreateUser(
	ctx context.Context,
	client *ent.Client,
	name string,
	age uint8,
) (*ent.User, error) {
	u, err := client.User.Create().
		SetAge(age).
		SetName(name).
		Save(ctx)

	if err != nil {
		return nil, fmt.Errorf("faild creating user: %v", err)
	}
	log.Printf("user was created: %v", u)
	return u, nil
}
