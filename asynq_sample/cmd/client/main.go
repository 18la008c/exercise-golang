package main

import (
	"log"
	"time"

	"github.com/hibiken/asynq"
	"gitlab.com/18la008c/exercise-golang/asynq_sample/tasks"
)

const redisAddr = "localhost:6379"

func main() {
	// redisにtaskを追加するcode　つまりclient側の処理
	// 実際に処理するのはworker
	client := asynq.NewClient(asynq.RedisClientOpt{Addr: redisAddr})
	defer client.Close()

	// taskの追加part1
	// 新規タスクを作成しqueueに追加する。追加したtaskはすぐに処理される
	// メインの処理と切り離して、非同期 + 分散して処理することができるのが強み！
	task, err := tasks.NewEmailDeliveryTask(1111, "some:template:id")
	if err != nil {
		log.Fatalf("could not create task: %v", err)
	}
	info, err := client.Enqueue(task)
	if err != nil {
		log.Fatalf("could not enqueue task: %v", err)
	}
	log.Printf("enqueued task: id=%s queue=%s", info.ID, info.Queue)

	// taskの追加part2
	// 新規タスクを作成しqueueに追加する。　ただし特定時間後に処理される。
	// 柔軟に処理時間を指定できるの強み！
	info, err = client.Enqueue(task, asynq.ProcessIn(24*time.Hour))
	if err != nil {
		log.Fatalf("could not schedule task: %v", err)
	}
	log.Printf("enqueued task: id=%s queue=%s", info.ID, info.Queue)

	// taskの追加part3
	// retry回数やtimeout時間を指定できるのが強み！
	task, err = tasks.NewImageResizeTask("https://example.com/myassets/image.jpg")
	if err != nil {
		log.Fatalf("could not create task: %v", err)
	}
	info, err = client.Enqueue(task, asynq.MaxRetry(10), asynq.Timeout(3*time.Minute))
	if err != nil {
		log.Fatalf("could not enqueue task: %v", err)
	}
	log.Printf("enqueued task: id=%s queue=%s", info.ID, info.Queue)
}
