package main

import (
	"log"

	"github.com/hibiken/asynq"
	"gitlab.com/18la008c/exercise-golang/asynq_sample/tasks"
)

const redisAddr = "localhost:6379"

func main() {
	server := asynq.NewServer(
		asynq.RedisClientOpt{Addr: redisAddr},
		asynq.Config{
			Concurrency: 10,
			Queues: map[string]int{
				"critical": 6,
				"default":  3,
				"low":      1,
			},
		},
	)

	mux := asynq.NewServeMux()
	mux.HandleFunc(tasks.TypeEmailDelivery, tasks.HandleEmailDeliveryTask)
	mux.Handle(tasks.TypeImageResize, tasks.NewImageProcessor())

	if err := server.Run(mux); err != nil {
		log.Fatalf("could not run server: %v", err)
	}
}
