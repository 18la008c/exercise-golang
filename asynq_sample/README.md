# 目的
- 公式のsampleを動かす
- https://github.com/hibiken/asynq

# 解説
- client.goを実行してredisにtaskを保存
```
go run cmd/client/main.go 
2024/03/09 04:44:22 enqueued task %!s(func() string=0x7f6860) with ID f011ff34-b735-43c8-ae08-fb81f9ec1777
2024/03/09 04:44:22 enqueued task: id=69baa23e-af78-4289-8730-f85187c8befa queue=default
2024/03/09 04:44:22 enqueued task: id=c9ae72f8-5310-4ff6-baa7-4c732842be74 queue=default
```

![img1](img/img1.png)
- workerを起動
```
go run cmd/worker/main.go 
asynq: pid=466870 2024/03/09 05:02:04.678247 INFO: Starting processing
asynq: pid=466870 2024/03/09 05:02:04.678292 INFO: Send signal TSTP to stop processing new tasks
asynq: pid=466870 2024/03/09 05:02:04.678298 INFO: Send signal TERM or INT to terminate the process
### ここからworkerの処理 ###
2024/03/09 05:02:18 delivering email to user 1111 with template some:template:id
2024/03/09 05:02:18 mock: sending email to user 1111
2024/03/09 05:02:18 resizing image from https://example.com/myassets/image.jpg
2024/03/09 05:02:18 mock: resizing image from https://example.com/myassets/image.jpg
```
- ちゃんと2件処理されたことがわかる。

![img2](img/img2.png)
- スケジュールもちゃんとされている(ただし左ペインのschedulerではないので注意)
    - http://localhost:8080/queues/default?status=scheduled
    
![img3](img/img3.png)
- その後処理されたtaskはredisから消える



