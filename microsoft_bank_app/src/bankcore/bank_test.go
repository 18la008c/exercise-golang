package bank

import "testing"

func TestAccount(t *testing.T) {
	//名前空間の取り扱いが良くわからないので調べる。 bank.Accountじゃなくて良い？
	account := Account{
		Customer: Customer{
			Name:    "Hiroki",
			Address: "TOKYO",
			Phone:   "090 1234 5678",
		},
		Number:  1001,
		Balance: 0,
	}

	if account.Name == "" {
		t.Error("can't create Account object")
	}

}
func TestDeposit(t *testing.T) {
	account := Account{
		Customer: Customer{
			Name:    "Hiroki",
			Address: "TOKYO",
			Phone:   "090 1234 5678",
		},
		Number:  1001,
		Balance: 0,
	}

	account.Deposit(10)
	if account.Balance != 10 {
		t.Error("balance is not being updated after a deposit")
	}

}

func TestDepositInvalid(t *testing.T) {
	account := Account{
		Customer: Customer{
			Name:    "Hiroki",
			Address: "TOKYO",
			Phone:   "090 1234 5678",
		},
		Number:  1001,
		Balance: 0,
	}

	// なんで err == nilかが分からない。。。 errが起きているんだから err != nilでしょ？
	if err := account.Deposit(-10); err == nil {
		t.Error("only positive numbers should be allowed to deposit")
	}
}

func TestWithdraw(t *testing.T) {
	account := Account{
		Customer: Customer{
			Name:    "Hiroki",
			Address: "TOKYO",
			Phone:   "090 1234 5678",
		},
		Number:  1001,
		Balance: 0,
	}
	account.Deposit(10)
	account.Withdraw(10)

	if account.Balance != 0 {
		t.Error("balance is not being updated after withdraw")
	}
}

func TestStatement(t *testing.T) {
	account := Account{
		Customer: Customer{
			Name:    "Hiroki",
			Address: "TOKYO",
			Phone:   "090 1234 5678",
		},
		Number:  1001,
		Balance: 0,
	}

	account.Deposit(100)
	statement := account.Statement()
	if statement != "1001 - Hiroki - 100" {
		t.Error("statement doesn't have the proper format")
	}
}

func TestSend(t *testing.T) {
	account := Account{
		Customer: Customer{
			Name:    "Hiroki",
			Address: "TOKYO",
			Phone:   "090 1234 5678",
		},
		Number:  1001,
		Balance: 100,
	}

	dest_account := Account{
		Customer: Customer{
			Name:    "TARO",
			Address: "SAITAMA",
			Phone:   "080 1234 5678",
		},
		Number:  1002,
		Balance: 100,
	}

	account.Send(&dest_account, 100)
	if (account.Balance != 0) || (dest_account.Balance != 200) {
		t.Error("balance is not being updated after send")
	}
}
