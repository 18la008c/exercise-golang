package main

import (
	"log"

	validation "github.com/go-ozzo/ozzo-validation/v4"
)

type Simple struct {
	Name string
	Age  uint8
}

func (s Simple) Validate() error {
	return validation.ValidateStruct(&s,
		validation.Field(&s.Name, validation.Required),
		validation.Field(
			&s.Age,
			validation.Required,
			validation.Min(uint8(10)), //uint8(10)のように、型指定が必須。　https://github.com/go-ozzo/ozzo-validation/issues/51
			validation.Max(uint(11)),
		),
	)
}

func main() {
	sOK := &Simple{
		Name: "okUser",
		Age:  11,
	}
	if err := sOK.Validate(); err != nil {
		log.Println(err)
	} else {
		log.Print("validataion is OK!!!")
	}

	sNG := &Simple{}
	if err := sNG.Validate(); err != nil {
		log.Printf("%T", err)
		log.Println(err)
	} else {
		log.Print("validataion is OK!!!")
	}
}

// go run main.go
// 2023/11/21 08:30:34 validataion is OK!!!
// 2023/11/21 08:30:34 validation.Errors
// 2023/11/21 08:30:34 Age: cannot be blank; Name: cannot be blank.
