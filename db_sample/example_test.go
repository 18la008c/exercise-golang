package todo

import (
	"context"
	"db_sample/ent"
	"db_sample/ent/todo"
	"fmt"
	"log"

	"entgo.io/ent/dialect"
	_ "github.com/mattn/go-sqlite3"
)

func ConnectDB() (context.Context, *ent.Client) {
	client, err := ent.Open(dialect.SQLite, "file:ent?mode=memory&cache=shared&_fk=1")
	// client, err := ent.Open(dialect.SQLite, "file:test.db?mode=rw&cache=shared&_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}

	ctx := context.Background()

	//schemaに応じて自動でmigration(table作成)してくれる
	if err := client.Schema.Create(ctx); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}
	return ctx, client
}

func Example_first() {
	ctx, client := ConnectDB()
	defer client.Close()

	task1, err := client.Todo.Create().SetText("Example1").Save(ctx)
	if err != nil {
		log.Fatalf("failed creating a todo: %v", err)
	}
	task2, err := client.Todo.Create().SetText("Example2").Save(ctx)
	if err != nil {
		log.Fatalf("failed creating a todo: %v", err)
	}
	fmt.Printf("%d: %q\n", task1.ID, task1.Text)
	fmt.Printf("%d: %q\n", task2.ID, task2.Text)
	// Output:
	// 1: "Example1"
	// 2: "Example2"
}

func Example_second() {
	ctx, client := ConnectDB()
	defer client.Close()

	_, err := client.Todo.Create().SetText("Example1").Save(ctx)
	if err != nil {
		log.Fatalf("failed creating a todo: %v", err)
	}
	_, err = client.Todo.Create().SetText("Example2").Save(ctx)
	if err != nil {
		log.Fatalf("failed creating a todo: %v", err)
	}

	// すべてのtodoアイテムを取得する
	items, err := client.Todo.Query().All(ctx)
	if err != nil {
		log.Fatalf("failed querying todos: %v", err)
	}
	for _, t := range items {
		fmt.Printf("%d: %q\n", t.ID, t.Text)
	}
	// Output:
	// 1: "Example1"
	// 2: "Example2"
}

func Example_third() {
	ctx, client := ConnectDB()
	defer client.Close()

	task1, err := client.Todo.Create().SetText("Example1").Save(ctx)
	if err != nil {
		log.Fatalf("failed creating a todo: %v", err)
	}
	task2, err := client.Todo.Create().SetText("Example2").Save(ctx)
	if err != nil {
		log.Fatalf("failed creating a todo: %v", err)
	}

	// リレーション
	if err := task2.Update().SetParent(task1).Exec(ctx); err != nil {
		log.Fatalf("failed connecting todo2 to its parent: %v", err)
	}

	// 他のtodoアイテムを親にもつtodoアイテムを全て取得する
	items, err := client.Todo.Query().Where(todo.HasParent()).All(ctx)
	if err != nil {
		log.Fatalf("failed querying todos: %v", err)
	}
	for _, t := range items {
		fmt.Printf("%d: %q\n", t.ID, t.Text)
	}
	// Output:
	// 2: "Example2"
}

func Example_forth() {
	ctx, client := ConnectDB()
	defer client.Close()

	task1, err := client.Todo.Create().SetText("Example1").Save(ctx)
	if err != nil {
		log.Fatalf("failed creating a todo: %v", err)
	}
	task2, err := client.Todo.Create().SetText("Example2").Save(ctx)
	if err != nil {
		log.Fatalf("failed creating a todo: %v", err)
	}

	// リレーション
	if err := task2.Update().SetParent(task1).Exec(ctx); err != nil {
		log.Fatalf("failed connecting todo2 to its parent: %v", err)
	}

	// 親はもたないが、子は持つtodoの抽出
	items, err := client.Todo.Query().
		Where(
			todo.Not(
				todo.HasParent(),
			),
			todo.HasChildren(),
		).
		All(ctx)
	if err != nil {
		log.Fatalf("failed querying todos: %v", err)
	}
	for _, t := range items {
		fmt.Printf("%d: %q\n", t.ID, t.Text)
	}
	// Output:
	// 1: "Example1"

}

func Example_fith() {
	ctx, client := ConnectDB()
	defer client.Close()

	task1, err := client.Todo.Create().SetText("Example1").Save(ctx)
	if err != nil {
		log.Fatalf("failed creating a todo: %v", err)
	}
	task2, err := client.Todo.Create().SetText("Example2").Save(ctx)
	if err != nil {
		log.Fatalf("failed creating a todo: %v", err)
	}

	// リレーション
	if err := task2.Update().SetParent(task1).Exec(ctx); err != nil {
		log.Fatalf("failed connecting todo2 to its parent: %v", err)
	}

	//子Todoを取得後、紐づいている親Todoを取得する
	parent, err := client.Todo.Query().
		Where(todo.HasParent()). // 親todoアイテムを持つtodoアイテムのみにフィルタリング
		QueryParent().           // 親todoアイテムについて走査を続ける
		Only(ctx)                // 1つのtodoアイテムのみ取得する
	if err != nil {
		log.Fatalf("failed querying todos: %v", err)
	}
	fmt.Printf("%d: %q\n", parent.ID, parent.Text)
	// Output:
	// 1: "Example1"
}
