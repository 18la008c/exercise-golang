- 以下をベースにJWTのtokenの作成と、検証をお試しする。
- https://zenn.dev/tychy/articles/fbd57d56225e6b

# sample
```
root@v0-dev-01:~/project/exercise-golang/auth_sample2# go run main.go 
mode: create
userID: 123 
&{mu:0xc00001a108 dc:<nil> options:0 audience:[] expiration:<nil> issuedAt:<nil> issuer:<nil> jwtID:<nil> notBefore:<nil> subject:<nil> privateClaims:map[]}
&{mu:0xc00001a108 dc:<nil> options:0 audience:[] expiration:<nil> issuedAt:<nil> issuer:<nil> jwtID:<nil> notBefore:<nil> subject:<nil> privateClaims:map[id:123]}
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMyJ9.HdEPYqE7fzGkBWBZyI-Cc8-gVQlNd2zBM7PwK47p67o
root@v0-dev-01:~/project/exercise-golang/auth_sample2# go run main.go 
mode: validate
token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEyMyJ9.HdEPYqE7fzGkBWBZyI-Cc8-gVQlNd2zBM7PwK47p67o
token is valid. your id: 123
root@v0-dev-01:~/project/exercise-golang/auth_sample2# go run main.go 
mode: validate
token: aaaa
panic: invalid JWT

goroutine 1 [running]:
main.validateToken({0xc0000c8180, 0x4})
        /root/project/exercise-golang/auth_sample2/main.go:41 +0x265
main.main()
        /root/project/exercise-golang/auth_sample2/main.go:77 +0x2ac
exit status 2
root@v0-dev-01:~/project/exercise-golang/auth_sample2# 

```