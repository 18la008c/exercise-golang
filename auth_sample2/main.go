package main

import (
	"fmt"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwt"
)

const (
	JWTSECRET = "secret"
)

func createToken(userID string) {
	// init token
	token := jwt.New()
	fmt.Printf("%+v\n", token)

	// set payload
	token.Set("id", userID)
	fmt.Printf("%+v\n", token)

	// set secret key
	key := jwt.WithKey(jwa.SignatureAlgorithm("HS256"), []byte(JWTSECRET))

	// Hash secret key
	sampleToken, err := jwt.Sign(token, key)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%+v\n", string(sampleToken))
}

func validateToken(tokenString string) {
	// TODO: 今回は生のpasswordを保存しているが、普通はhash化したものを使ってる？

	key := jwt.WithKey(jwa.SignatureAlgorithm("HS256"), []byte(JWTSECRET))

	t, err := jwt.ParseString(tokenString, key)
	if err != nil {
		panic(err)
	}

	if err := jwt.Validate(t); err != nil {
		panic(err)
	}

	id, ok := t.Get("id")
	if !ok {
		panic("no id found")
	}

	fmt.Printf("token is valid. your id: %s\n", id)
}

func main() {
	// input mode(create or validate) from stdin
	var mode string
	fmt.Print("mode: ")
	fmt.Scan(&mode)
	if mode != "create" && mode != "validate" {
		panic("invalid mode")
	}

	if mode == "create" {
		var userID string
		fmt.Print("userID: ")
		fmt.Scan(&userID)
		createToken(userID)
		return
	}

	if mode == "validate" {
		var token string
		fmt.Print("token: ")
		fmt.Scan(&token)
		validateToken(token)
		return
	}
}
