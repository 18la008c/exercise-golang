package main

import (
	"context"
	"database/sql"
	"db_sample6/internal/config"
	"fmt"
	"log"

	entsql "entgo.io/ent/dialect/sql"
	"github.com/go-sql-driver/mysql"

	"db_sample6/ent"
)

func NewSQLClient(cfg *config.Database) (*sql.DB, error) {
	c := mysql.Config{
		User:   cfg.User,
		Passwd: cfg.Password,
		Net:    "tcp",
		Addr:   fmt.Sprintf("%s:%d", cfg.Host, cfg.Port),
		DBName: cfg.Name,
	}
	db, err := sql.Open(cfg.Driver, c.FormatDSN())
	if err != nil {
		return nil, fmt.Errorf("cannot connect db: %v", err)
	}

	if err := db.Ping(); err != nil {
		return nil, fmt.Errorf("error pinging to db:%v", err)
	}
	log.Printf("Database connection established successfully!")
	return db, nil
}

func NewEntClient(db *sql.DB) (*ent.Client, error) {
	// create ent client from sql object
	drv := entsql.OpenDB("mysql", db)
	return ent.NewClient(ent.Driver(drv)), nil
}

func main() {
	cfg := config.New()
	ctx := context.Background()
	dbClient, err := NewSQLClient(cfg.Database)
	if err != nil {
		log.Fatalf("Faild to connect db: %v", err)
	}

	entClient, err := NewEntClient(dbClient)
	if err != nil {
		log.Fatalf("Faild to connect db: %v", err)
	}

	// auto migrate database by /ent/schema
	if err := entClient.Schema.Create(ctx); err != nil {
		log.Fatalf("Failed creating schema resources: %v", err)
	}

	entClient.Close()                       // entをclose
	if err := dbClient.Ping(); err != nil { // close後にDBへping
		log.Printf("error pinging to db:%v", err)
	}
}

// entを閉じれば、sql.DBも閉じられる！
// root@035c3063452c:/app# go run main.go
// 2023/11/17 07:52:04 Database connection established successfully!
// 2023/11/17 07:52:04 error pinging to db:sql: database is closed
