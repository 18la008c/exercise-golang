package middleware

import (
	"log"
	"log_sample3/contextmanager"
	"log_sample3/logging"
	"net"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"
)

func SetRequestUUID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()

		// uuid set to ctx
		uuid := uuid.New().String()
		ctx = contextmanager.SetRequestUUID(ctx, uuid)

		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func PrintRequestUUID(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		uuid := contextmanager.GetRequestUUID(ctx)
		log.Print(uuid)

		next.ServeHTTP(w, r)
	})
}

type ResponseWriterWrapper struct {
	http.ResponseWriter
	statusCode int
}

func (w *ResponseWriterWrapper) WriteHeader(statusCode int) {
	w.statusCode = statusCode
	w.ResponseWriter.WriteHeader(statusCode)
}

func SetLogger(logger *logging.AccessLogger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			ctx := contextmanager.SetLogger(r.Context(), logger)
			r = r.WithContext(ctx)
			writer := &ResponseWriterWrapper{ResponseWriter: w} //TODO: 変数名を入れると省略がが許される
			start := time.Now()
			next.ServeHTTP(writer, r)

			logger.Log(
				r,
				contextmanager.GetRequestUUID(r.Context()),
				getIPFromHeader(r),
				writer.statusCode,
				time.Since(start),
			)
		})
	}
}

func getIPFromHeader(r *http.Request) string {
	if ip := r.Header.Get("X-Forwarded-For"); ip != "" {
		return strings.TrimSpace(strings.Split(ip, ",")[0])
	}
	if ip := r.Header.Get("X-Real-Ip"); ip != "" {
		return ip
	}
	ip, _, _ := net.SplitHostPort(r.RemoteAddr)
	return ip
}
