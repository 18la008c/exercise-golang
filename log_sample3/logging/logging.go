package logging

import (
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"path"
	"strings"
	"time"
)

var (
	appEnv       = os.Getenv("APP_ENV")
	SystemLogger = NewSystemLogger()
	AcLogger     = NewAccessLogger()
)

func initOptions(loglevel slog.Level) *slog.HandlerOptions {
	return &slog.HandlerOptions{
		AddSource: true,
		Level:     loglevel,
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			switch a.Key {
			case slog.TimeKey:
				const layout = "2006-01-02 03:04:05"
				t := a.Value.Time()
				jst := time.FixedZone("Asia/Tokyo", 9*60*60)
				a.Value = slog.StringValue(t.In(jst).Format(layout))
			case slog.SourceKey:
				s := a.Value.Any().(*slog.Source)
				s.File = path.Base(s.File)
				a.Value = slog.StringValue(fmt.Sprintf("%s:%d", s.File, s.Line))
			}
			if strings.Contains(a.Value.String(), "passw@rd") {
				newStr := strings.ReplaceAll(a.Value.String(), "passw@rd", "********")
				a.Value = slog.StringValue(newStr)
			}
			return a
		},
	}
}

func newLogger() *slog.Logger {
	if appEnv == "production" || appEnv == "prod" {
		handler := slog.NewJSONHandler(os.Stdout, initOptions(slog.LevelInfo))
		return slog.New(handler)

	} else {
		handler := slog.NewTextHandler(os.Stdout, initOptions(slog.LevelDebug))
		return slog.New(handler)
	}
}

func NewSystemLogger() *slog.Logger {
	logger := newLogger()
	return logger.With(slog.String("service", "system"))
}

type AccessLogger struct {
	logger *slog.Logger
}

func NewAccessLogger() *AccessLogger {
	logger := newLogger()
	return &AccessLogger{logger: logger.With(slog.String("service", "access"))}
}

func (l *AccessLogger) Log(
	r *http.Request,
	uuid string,
	remote string,
	status int,
	elapsed time.Duration,
) {
	l.logger.Info(
		"access",
		slog.String("method", r.Method),
		slog.String("path", r.URL.Path),
		slog.String("remote", remote),
		slog.String("user_agent", r.UserAgent()),
		slog.String("uuid", uuid),
		slog.Int("status", status),
	)
}
