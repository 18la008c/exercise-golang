package contextmanager

import (
	"context"
	"log_sample3/logging"
)

type contextKey string

const (
	requestUUIDKey contextKey = "requestUUID"
	loggerKey      contextKey = "logger"
)

func SetRequestUUID(ctx context.Context, uuid string) context.Context {
	return context.WithValue(ctx, requestUUIDKey, uuid)
}

func GetRequestUUID(ctx context.Context) string {
	uuid, ok := ctx.Value(requestUUIDKey).(string)
	if !ok {
		return ""
	}
	return uuid
}

func SetLogger(ctx context.Context, logger *logging.AccessLogger) context.Context {
	return context.WithValue(ctx, loggerKey, logger)
}
