package main

import (
	"log_sample3/logging"
	"log_sample3/middleware"
	"net/http"

	"github.com/go-chi/chi/v5"
	// "github.com/go-chi/chi/v5/middleware"
)

func main() {
	r := chi.NewRouter()
	r.Use(middleware.SetRequestUUID)
	r.Use(middleware.SetLogger(logging.AcLogger))
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("welcome\n"))
	})
	http.ListenAndServe(":3000", r)
}

// 独自のLog()を実装すれば、middleware.Loggerの代わりに使える(statusが0だが、これはstatusを何も指定してないから)
// time="2024-01-05 02:17:51" level=INFO source=logging.go:76 msg=access service=access method=GET path=/ remote=127.0.0.1 user_agent=curl/7.81.0 uuid=d3341525-8adc-4aae-99ef-161d5b234d0e status=0
