package authentication

import (
	"log"
	"strconv"
	"time"

	"github.com/lestrrat-go/jwx/v2/jwa"
	"github.com/lestrrat-go/jwx/v2/jwt"
)

const (
	TokenExpireDuration = 30 * time.Second
)

var SecretKey = jwt.WithKey(
	jwa.SignatureAlgorithm(jwa.HS256),
	[]byte("secret"),
)

func GenerateToken(userID int) (string, error) {
	token := jwt.New()
	if err := token.Set(jwt.IssuedAtKey, time.Now().UTC().Unix()); err != nil {
		log.Printf("failed to set issuedAt: %+v", err)
		return "", err
	}
	if err := token.Set(jwt.ExpirationKey, time.Now().UTC().Add(TokenExpireDuration).Unix()); err != nil {
		log.Printf("failed to set expiration: %+v", err)
		return "", err
	}
	if err := token.Set(jwt.SubjectKey, strconv.Itoa(userID)); err != nil {
		log.Printf("failed to set subject: %+v", err)
		return "", err
	}
	log.Printf("token: %+v", token)

	signedToken, err := jwt.Sign(token, SecretKey)
	if err != nil {
		return "", err
	}
	return string(signedToken), nil
}
