package authentication

import (
	"auth_sample4/ent"
	"auth_sample4/internal/domain/user"
	"auth_sample4/internal/utils/message"
	"auth_sample4/internal/utils/response"
	"encoding/json"
	"log"
	"net/http"
)

type Handler struct {
	repository *user.Repository
}

func NewHandler(repository *user.Repository) *Handler {
	return &Handler{
		repository: repository,
	}
}

func (h *Handler) Token(w http.ResponseWriter, r *http.Request) {
	var req TokenRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		response.Error(w, http.StatusBadRequest, err)
		return
	}

	user, err := h.repository.FindByEmail(r.Context(), req.Email)
	if err != nil {
		log.Println(err)
		if ent.IsNotFound(err) {
			response.Error(w, http.StatusNotFound, message.ErrNoRecord)
			return
		}
		response.Error(w, http.StatusInternalServerError, err)
		return
	}

	if user.Password != req.Password {
		response.Error(w, http.StatusForbidden, message.ErrForbidden)
		return
	}

	token, err := GenerateToken(user.ID)
	if err != nil {
		response.Error(w, http.StatusInternalServerError, err)
		return
	}

	response.JSON(w, http.StatusOK, TokenResponse{Token: token})
}
