package authentication

import (
	"context"
	"log"
	"net/http"
	"strings"

	"github.com/lestrrat-go/jwx/v2/jwt"
)

func Authenticator(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString := r.Header.Get("Authorization")
		if tokenString == "" {
			log.Printf("token not found")
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		tokenString = strings.Replace(tokenString, "Bearer ", "", 1)

		token, err := jwt.ParseString(tokenString, SecretKey)
		if err != nil {
			log.Printf("failed to parse token: %+v", err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		if err := jwt.Validate(token); err != nil {
			log.Printf("token is invalid: %+v", err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		log.Printf("token: %+v", token)

		userID, ok := token.Get("sub")
		if !ok {
			log.Printf("sub not found: %+v", userID)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		log.Printf("userID: %+v", userID)
		ctx := context.WithValue(r.Context(), "userID", userID.(string))
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
