package user

type User struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Name     string `json:"name"`
}

func NewUser(id int, email, password, name string) *User {
	return &User{
		ID:       id,
		Email:    email,
		Password: password,
		Name:     name,
	}
}

func (u *User) IsSamePassword(password string) bool {
	return u.Password == password
}
