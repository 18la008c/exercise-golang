package user

import (
	"log"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"

	"auth_sample4/ent"
	"auth_sample4/internal/utils/message"
	"auth_sample4/internal/utils/response"
)

type Handler struct {
	repository *Repository
}

func NewHandler(repository *Repository) *Handler {
	return &Handler{
		repository: repository,
	}
}

func (h *Handler) List(w http.ResponseWriter, r *http.Request) {
	users, err := h.repository.List(r.Context())
	if err != nil {
		log.Println(err)
		response.Error(w, http.StatusInternalServerError, err)
		return
	}
	response.JSON(w, http.StatusOK, users)
}

func (h *Handler) Get(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(chi.URLParam(r, "id"))
	if err != nil {
		response.Error(w, http.StatusBadRequest, message.ErrBadRequest)
		return
	}

	user, err := h.repository.FindByID(r.Context(), userID)
	if err != nil {
		if ent.IsNotFound(err) {
			response.Error(w, http.StatusNotFound, message.ErrNoRecord)
			return
		}
		response.Error(w, http.StatusInternalServerError, err)
		return
	}

	response.JSON(w, http.StatusOK, user)
}
