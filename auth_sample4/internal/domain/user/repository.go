package user

import (
	"context"

	"auth_sample4/ent"
	"auth_sample4/ent/user"
)

type Repository struct {
	entClient *ent.Client
}

func NewRepository(entClient *ent.Client) *Repository {
	return &Repository{
		entClient: entClient,
	}
}

func (r *Repository) FindByEmail(ctx context.Context, email string) (*User, error) {
	u, err := r.entClient.User.Query().Where(user.Email(email)).Only(ctx)

	return NewUser(u.ID, u.Email, u.Password, u.Name), err
}

func (r *Repository) FindByID(ctx context.Context, id int) (*User, error) {
	u, err := r.entClient.User.Get(ctx, id)

	return NewUser(u.ID, u.Email, u.Password, u.Name), err
}

func (r *Repository) List(ctx context.Context) ([]*User, error) {
	users, err := r.entClient.User.Query().All(ctx)
	if err != nil {
		return nil, err
	}

	var result []*User
	for _, u := range users {
		result = append(result, NewUser(u.ID, u.Email, u.Password, u.Name))
	}

	return result, nil
}
