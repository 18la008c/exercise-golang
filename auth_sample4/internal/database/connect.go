package database

import (
	"context"
	"log"

	"auth_sample4/ent"

	_ "github.com/mattn/go-sqlite3"
)

func init() {
	ctx := context.Background()
	entClient, err := NewEntClient()
	if err != nil {
		log.Fatal(err)
	}
	if err := entClient.Schema.Create(ctx); err != nil {
		log.Fatal(err)
	}

	if err := entClient.User.Create().
		SetEmail("alpha@example.com").
		SetPassword("AlphaPasswo@rd").
		SetName("alpha").
		Exec(ctx); err != nil {
		log.Fatal(err)
	}

	if err := entClient.User.Create().
		SetEmail("bravo@example.com").
		SetPassword("BravoPasswo@rd").
		SetName("bravo").
		Exec(ctx); err != nil {
		log.Fatal(err)
	}

	if err := entClient.User.Create().
		SetEmail("charlie@example.com").
		SetPassword("CharliePasswo@rd").
		SetName("charlie").
		Exec(ctx); err != nil {
		log.Fatal(err)
	}
}

func NewEntClient() (*ent.Client, error) {
	entClient, err := ent.Open("sqlite3", "file:ent?mode=memory&cache=shared&_fk=1")
	if err != nil {
		return nil, err
	}
	return entClient, nil
}
