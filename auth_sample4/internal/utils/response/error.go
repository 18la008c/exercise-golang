package response

import (
	"encoding/json"
	"log"
	"net/http"
)

func Error(w http.ResponseWriter, statusCode int, message error) {
	w.Header().Set("Content-Type", "application/problem+json")
	w.WriteHeader(statusCode)

	if message == nil {
		write(w, nil)
		return
	}

	p := map[string]string{
		"message": message.Error(),
	}

	data, err := json.Marshal(p)
	if err != nil {
		log.Println(err)
	}

	write(w, data)
}

func write(w http.ResponseWriter, data []byte) {
	if _, err := w.Write(data); err != nil {
		log.Println(err)
	}
}
