package server

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"

	"auth_sample4/ent"
	"auth_sample4/internal/database"
	"auth_sample4/internal/domain/authentication"
	"auth_sample4/internal/domain/user"
	"auth_sample4/internal/utils/response"
)

type Server struct {
	entClient  *ent.Client
	router     *chi.Mux
	httpServer *http.Server
}

func New() (*Server, error) {
	entClient, err := database.NewEntClient()
	if err != nil {
		return nil, fmt.Errorf("Failed to connect: %w", err)
	}

	return &Server{
		entClient: entClient,
		router:    chi.NewRouter(),
	}, nil
}

func (s *Server) Init() {
	s.initRouter()
}

func (s *Server) Run() {
	log.Print("run")
	s.httpServer = &http.Server{
		Addr:    ":8080",
		Handler: s.router,
	}

	log.Printf("Serving at :8080")
	err := s.httpServer.ListenAndServe()
	if err != nil {
		log.Fatal(err)
	}
}

func (s *Server) initRouter() {
	userRepository := user.NewRepository(s.entClient)
	userHandler := user.NewHandler(userRepository)
	authenticationHandler := authentication.NewHandler(userRepository)

	s.router.Use(middleware.Logger)

	//auth group
	s.router.Group(func(r chi.Router) {
		r.Use(authentication.Authenticator)
		r.Get("/users", userHandler.List)
		r.Get("/users/{id}", userHandler.Get)
	})

	//not auth group
	s.router.Group(func(r chi.Router) {
		r.Post("/auth/token", authenticationHandler.Token)
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			response.JSON(w, http.StatusOK, map[string]string{"message": "welcome"})
		})
	})
}
