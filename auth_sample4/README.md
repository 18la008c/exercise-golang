# 目的
- 認証の仕組みのある程度完成形
- 残りは、refreshtokenとか

# use
- create token
```
root@v0-dev-01:~/project/exercise-golang/auth_sample4# curl -i -X POST \
  -H "Content-Type: application/json" \
  -d '{"email": "alpha@example.com", "password": "AlphaPasswo@rd"}' \
  localhost:8080/auth/token
HTTP/1.1 200 OK
Content-Type: application/json
Date: Fri, 29 Dec 2023 04:20:35 GMT
Content-Length: 153

{"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MDM4MjM2NjUsImlhdCI6MTcwMzgyMzYzNSwic3ViIjoiMSJ9.pRoNn1GIseOcZNvwRU-ij3tY1S5vtYuGizq5qMht4H4"}
```
- verify token
```
root@v0-dev-01:~/project/exercise-golang/auth_sample4# curl -i -X GET \
-H "Content-Type: application/json" \
-H "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MDM4MjM0MjcsImlhdCI6MTcwMzgyMzM5Nywic3ViIjoiMSJ9.thkmbUxFiZ4ztFyDs_zWm2gFNM5hWc9QEc5qWn3NRpA" \
localhost:8080/users
HTTP/1.1 200 OK
Content-Type: application/json
Date: Fri, 29 Dec 2023 04:17:05 GMT
Content-Length: 247

[{"id":1,"email":"alpha@example.com","password":"AlphaPasswo@rd","name":"alpha"},{"id":2,"email":"bravo@example.com","password":"BravoPasswo@rd","name":"bravo"},{"id":3,"email":"charlie@example.com","password":"CharliePasswo@rd","name":"charlie"}]
```

# 仕組み
1. `POST /token`でuser/pass認証の実施
2. 認証がOKであればJWTを発行
3. JWTを受け取り、APIserverのSecretKeyで復号してheaderとpayloadを検証
4. 問題なければ、user/passの代わりに認証OKとする