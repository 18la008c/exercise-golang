package main

import (
	"log"

	"auth_sample4/internal/server"
)

func main() {
	s, err := server.New()
	if err != nil {
		log.Fatalf("Cannot create server instance: %v", err)
	}
	s.Init()
	s.Run()
}
