package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

type JSONTime struct {
	Date time.Time `json:"date"`
}

func main() {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("welcome"))
	})
	r.Post("/unmarshal", CreateByUnMarshal)
	r.Post("/decode", CreateByDecode)

	http.ListenAndServe(":3000", r)
}

func CreateByUnMarshal(w http.ResponseWriter, r *http.Request) {
	var req JSONTime

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println(err)
		w.Write([]byte(fmt.Sprintf("%s", err)))
	}
	log.Printf("%+v", req)

	data, err := json.Marshal(&req)
	if err != nil {
		w.Write([]byte(fmt.Sprintf("%s", err)))
	}

	w.Write(data)
}

func CreateByDecode(w http.ResponseWriter, r *http.Request) {
	var req JSONTime

	body, _ := io.ReadAll(r.Body)
	if err := json.Unmarshal(body, &req); err != nil {
		log.Println(err)
		w.Write([]byte(fmt.Sprintf("%s", err)))
	}
	log.Printf("%v", req.Date)

	data, err := json.Marshal(&req)
	if err != nil {
		w.Write([]byte(fmt.Sprintf("%s", err)))
	}

	w.Write(data)
}
