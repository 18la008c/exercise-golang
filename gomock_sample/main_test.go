package main

import (
	"testing"

	gomock "go.uber.org/mock/gomock"
)

func TestMockUserFind(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockRepo := NewMockUserRepository(ctrl)
	mockRepo.EXPECT().FindByName("test").Return(User{Name: "test"}, nil)

	user, err := mockRepo.FindByName("test")
	if err != nil {
		t.Fatal(err)
	}
	if user.Name != "test" {
		t.Fatalf("expected %s but got %s", "test", user.Name)
	}
}

func TestStubUserFind(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()

	mockRepo := NewMockUserRepository(ctrl)

	// Stub the FindByName method
	mockRepo.EXPECT().FindByName("test").Return(User{Name: "test"}, nil) // AnyTimesをつけるとstubになる
	mockRepo.EXPECT().FindByName("testx").Return(User{Name: "testx"}, nil).AnyTimes()

	user, err := mockRepo.FindByName("testx")
	if err != nil {
		t.Fatal(err)
	}
	if user.Name != "testx" {
		t.Fatalf("expected %s but got %s", "testx", user.Name)
	}

}
