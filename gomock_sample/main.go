package main

type User struct {
	Name string
}

type UserRepository interface {
	FindByName(name string) (User, error)
}
