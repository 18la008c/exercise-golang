package main

import (
	"fmt"

	"github.com/google/uuid"
)

type BookID uuid.UUID

type BookIDWithStr uuid.UUID

func (id BookIDWithStr) String() string {
	return uuid.UUID(id).String()
}

func main() {
	uuid := uuid.New()
	bid := BookID(uuid)
	fmt.Println(bid)

	bid2 := BookIDWithStr(uuid)
	fmt.Println(bid2)
}
