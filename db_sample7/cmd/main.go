package main

import (
	"fmt"

	"github.com/google/uuid"
)

type BookID uuid.UUID

func NewBookID() BookID {
	return BookID(uuid.New())
}

func (id BookID) String() string {
	return uuid.UUID(id).String()
}

func echoBookID(id BookID) {
	fmt.Print(id)
}

func echoUUID(id uuid.UUID) {
	fmt.Print(id)

}

func main() {
	BookID := NewBookID()
	echoBookID(BookID)
	echoUUID(uuid.UUID(BookID))
}
