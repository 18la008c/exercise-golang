package main

import (
	"context"
	"db_sample8/ent"
	"log"

	"entgo.io/ent/dialect"
	"github.com/google/uuid"
	_ "github.com/mattn/go-sqlite3"
)

type BookID uuid.UUID

func (id BookID) String() string { // Stringを実装していると、%v, %sで出力した時にこれを使ってくれる。
	return uuid.UUID(id).String()
}

func (id BookID) UUID() uuid.UUID {
	return uuid.UUID(id)
}

type BookModel struct {
	ID    BookID
	Title string
}

func main() {
	client, err := ent.Open(dialect.SQLite, "file:ent?mode=memory&cache=shared&_fk=1")
	// client, err := ent.Open(dialect.SQLite, "file:sample.db?_fk=1")
	if err != nil {
		log.Fatalf("failed opening connection to sqlite: %v", err)
	}
	defer client.Close()

	ctx := context.Background()
	if err := client.Schema.Create(ctx); err != nil {
		log.Fatalf("failed creating schema resources: %v", err)
	}

	// Create a new book with "The Go Programming Language" as title.
	book, err := client.Book.
		Create().
		SetTitle("The Go Programming Language").
		Save(ctx)
	if err != nil {
		log.Fatalf("failed creating book: %v", err)
	}
	log.Println("book was created: ", book)

	// Query a book with a bookid
	book, err = client.Book.Get(ctx, uuid.New())

	if err != nil {
		log.Fatalf("failed querying book: %v", err)
	}
	log.Println("book was queried: ", book)

	// to BookModel
	bookModel := BookModel{
		ID:    BookID(book.ID),
		Title: book.Title,
	}
	log.Println("bookModel: ", bookModel)
}
