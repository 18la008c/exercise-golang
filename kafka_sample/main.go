package main

import (
	"encoding/json"

	"github.com/IBM/sarama"
)

type Sample struct {
	Name string `json:"name"`
	Age  int8   `json:"age"`
}

func main() {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer(
		[]string{"kafka:9092"},
		config,
	)
	if err != nil {
		panic(err)
	}
	sample := Sample{
		Name: "hiroki",
		Age:  99,
	}

	json, err := json.Marshal(sample)
	if err != nil {
		panic(err)
	}

	msg := &sarama.ProducerMessage{
		Topic: "sample",
		Key:   sarama.StringEncoder("abcdefg"),
		Value: sarama.StringEncoder(json),
	}
	producer.SendMessage(msg)

}

//kafkaのcontainerからcliで確認もできる。 + 自動で待ち受けモードに入る
//I have no name!@c2b8be601f85:/bin$ kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic sample --from-beginning
//
//{"name":"hiroki","age":99}
//{"name":"hiroki","age":99}
