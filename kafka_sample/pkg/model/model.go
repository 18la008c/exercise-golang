package model

type User struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type Notification struct {
	From    User   `json:"from"`
	To      User   `json:"user"`
	Message string `json:"message"`
}
