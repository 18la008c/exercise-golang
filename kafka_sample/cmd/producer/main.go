package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/IBM/sarama"
	"github.com/gin-gonic/gin"

	"gitlab.com/18la008c/exercise-golang/kafka_sample/pkg/model"
)

const (
	ProducerPort       = ":8881"
	KafkaServerAddress = "kafka:9092"
	KafkaTopic         = "notifications"
)

// error strings should not be capitalized (ST1005)
// https://stackoverflow.com/questions/68792696/why-error-messages-shouldnt-end-with-a-punctuation-mark-in-go
var ErrUserNotFoundInProducer = errors.New("user not found")

func findUserById(id int, users []model.User) (model.User, error) {
	for _, user := range users {
		if user.Id == id {
			return user, nil
		}
	}

	return model.User{}, ErrUserNotFoundInProducer
}

func getIdFromRequest(formValue string, ctx *gin.Context) (int, error) {
	id, err := strconv.Atoi(ctx.PostForm(formValue))
	if err != nil {
		return 0, fmt.Errorf("failed to parse Id from form value %s: %w", formValue, err)
	}
	return id, err

}

func sendKafkaMessage(
	producer sarama.SyncProducer,
	users []model.User,
	ctx *gin.Context,
	fromId, toId int,
) error {
	message := ctx.PostForm("message")
	fromUser, err := findUserById(fromId, users)
	if err != nil {
		return err
	}

	toUser, err := findUserById(toId, users)
	if err != nil {
		return err
	}

	notification := model.Notification{
		From:    fromUser,
		To:      toUser,
		Message: message,
	}

	notificationJson, err := json.Marshal(notification)
	if err != nil {
		return fmt.Errorf("failed to marshal notification: %w", err)
	}

	msg := &sarama.ProducerMessage{
		Topic: KafkaTopic,
		Key:   sarama.StringEncoder(strconv.Itoa(toUser.Id)),
		Value: sarama.StringEncoder(notificationJson),
	}
	_, _, err = producer.SendMessage(msg)
	return err
}

func sendMessageHandler(
	producer sarama.SyncProducer,
	users []model.User,
) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		fromId, err := getIdFromRequest("fromId", ctx)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		toId, err := getIdFromRequest("toId", ctx)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, gin.H{"message": err.Error()})
			return
		}

		err = sendKafkaMessage(producer, users, ctx, fromId, toId)
		if errors.Is(err, ErrUserNotFoundInProducer) {
			ctx.JSON(http.StatusNotFound, gin.H{"message": "User not found"})
			return
		}
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, gin.H{
				"message": err.Error(),
			})
			return
		}

		ctx.JSON(http.StatusOK, gin.H{
			"message": "Notification sent successfully!",
		})
	}
}

func setupProducer() (sarama.SyncProducer, error) {
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	producer, err := sarama.NewSyncProducer([]string{KafkaServerAddress},
		config)
	if err != nil {
		return nil, fmt.Errorf("failed to setup producer: %w", err)
	}
	return producer, nil
}

func main() {
	users := []model.User{
		{Id: 1, Name: "Emma"},
		{Id: 2, Name: "Bruno"},
		{Id: 3, Name: "Rick"},
		{Id: 4, Name: "Lena"},
	}

	producer, err := setupProducer()
	if err != nil {
		log.Fatalf("failed to initialize producer: %v", err)
	}
	defer producer.Close()

	gin.SetMode(gin.ReleaseMode)
	router := gin.Default()
	router.GET("/",
		func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, gin.H{"message": "hello"})
		},
	)
	router.POST("/send", sendMessageHandler(producer, users))

	fmt.Printf("Kafka PRODUCER 📨 started at http://kafka%s\n", ProducerPort)

	if err := router.Run(ProducerPort); err != nil {
		log.Printf("failed to run the server: %v", err)
	}
}

//curl -X POST 192.168.1.203:8881/send -d "fromId=2&toId=1&message=Bruno started following you."
