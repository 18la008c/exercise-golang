package main

import (
	"log"
	"net/http"
	"os"

	"github.com/go-chi/chi/v5"
)

func main() {
	port := "8080"

	if fromEnv := os.Getenv("PORT"); fromEnv != "" {
		port = fromEnv
	}

	log.Printf("Starting up on http://localhost:%s", port)

	r := chi.NewRouter()

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/plain")
		w.Write([]byte("Hello World!")) //書き込みにはbyteである必要がある。
	})

	r.Mount("/posts", postsResource{}.Routes())

	//log.Fatalを使うことで、errが起きた時にlogging + exit(1)ができる
	//https://maku77.github.io/p/goruwy4/
	log.Fatal(http.ListenAndServe(":"+port, r))
}
