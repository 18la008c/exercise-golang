# Chiのsample1
- https://www.newline.co/@kchan/building-a-simple-restful-api-with-go-and-chi--5912c411


# 環境構築
```
root@v0-dev-01:~/project/exercise-golang/api_sample2# go mod init api_sample2
go: creating new go.mod: module api_sample2

root@v0-dev-01:~/project/exercise-golang/api_sample2# go get -u github.com/go-chi/chi/v5

go: downloading github.com/go-chi/chi/v5 v5.0.10
go: added github.com/go-chi/chi/v5 v5.0.10
```